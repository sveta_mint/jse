package by.epam.impl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * Class handler for proxy object
 */
public class InvocationHandlerImpl implements InvocationHandler {
    private final IShape iShape;

    /**
     * Constructor for create handler object
     * @param iShape interface of shape
     */
    public InvocationHandlerImpl(final IShape iShape) {
        this.iShape = iShape;
    }

    @Override
    public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
        return method.invoke(iShape, args);
    }

}
