package by.epam.impl;

import by.epam.annotations.Proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;

/**
 * Class that creates shapes based on the presence of annotation
 */
public class ShapeFactory {
    /**
     * method to get instance
     * @param aClass Class shape
     * @param nameShape name of shape
     * @return instance of shape
     * @throws IllegalAccessException when an application tries to reflectively
     * create an instance (other than an array)
     * @throws InstantiationException when an application tries to create an instance of a class using
     * the newInstance method in class Class, but the specified class object cannot be instantiated
     * @throws NoSuchMethodException when a certain method cannot be found.
     * @throws InvocationTargetException is a checked exception that wraps an exception
     * thrown by a called method or constructor
     */
    public Object getInstanceOf(final Class<?> aClass, final String nameShape) throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        Object object = aClass.getConstructor(String.class).newInstance(nameShape);
        if (aClass.isAnnotationPresent(Proxy.class)) {
            System.out.println("Create proxy object!");
            Class handler = (aClass.getAnnotation(Proxy.class)).invocationHandler();
            object = java.lang.reflect.Proxy.newProxyInstance(aClass.getClassLoader(), aClass.getInterfaces(),
                    (InvocationHandler) handler.getConstructor(IShape.class).newInstance(aClass.getConstructor(String.class).newInstance(nameShape)));
        } else {
            System.out.println("Create simple object!");
        }
        return object;
    }
}
