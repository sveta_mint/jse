package by.epam.impl;

import by.epam.annotations.Equal;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Utility class for comparing two objects
 */
public final class EqualsObject {
    /**
     * Enum contains the type of object comparisons
     */
    public enum Compareby {
        REFERENCE, VALUE
    }
    private EqualsObject() {

    }
    /**
     * Method for comparing objects for equality
     * @param obj1 first object
     * @param obj2 second object
     * @return the result of the comparison objects
    */
    public static boolean equalObjects(final Object obj1, final Object obj2) {
        if (obj1 == obj2) {
            return true;
        }

        if (obj1 == null || obj2 == null || obj1.getClass() != obj2.getClass()) {
            return false;
        }

        return checkEqualsFields(obj1, obj2);
    }

    private static boolean checkEqualsFields(final Object obj1, final Object obj2) {
        Field[] fields = obj1.getClass().getDeclaredFields();
        boolean result = false;
        for (Field field : fields) {
            if (field.isAnnotationPresent(Equal.class) && field.getAnnotation(Equal.class).type() == Compareby.VALUE) {
                result = compareByValue(obj1, obj2, field);
            } else {
                if (field.isAnnotationPresent(Equal.class)) {
                    result = compareByReference(obj1, obj2, field);
                }
            }
        }
        return result;
    }

    private static boolean compareByValue(final Object obj1, final Object obj2, final Field field) {
        try {
            field.setAccessible(true);
            boolean result = field.get(obj1).equals(field.get(obj2));
            field.setAccessible(false);
            return result;
        } catch (IllegalAccessException e) {
            throw new IllegalStateException(e);
        }
    }

    private static boolean compareByReference(final Object obj1, final Object obj2, final Field field) {
        try {
            field.setAccessible(true);
            boolean result = field.get(obj1) == field.get(obj2);
            field.setAccessible(false);
            return result;
        } catch (IllegalAccessException e) {
            throw new IllegalStateException(e);
        }
    }
}
