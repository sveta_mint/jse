package by.epam.impl;

/**
 * Interface describing the name of the shape
 */
public interface IShape {
    /**
     *
     * @return name shape
     */
    String getName();
}
