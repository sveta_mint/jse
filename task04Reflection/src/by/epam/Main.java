package by.epam;

import by.epam.beans.Circle;
import by.epam.impl.EqualsObject;
import by.epam.beans.Rectangle;
import by.epam.impl.ShapeFactory;

import java.lang.reflect.InvocationTargetException;

/**
 * Runner for task
 */
public final class Main {
    /**
     * Method the entry point to the program
     * @param args array of strings
     * @throws IllegalAccessException when an application tries to reflectively
     * create an instance (other than an array)
     * @throws InstantiationException when an application tries to create an instance of a class using
     * the newInstance method in class Class, but the specified class object cannot be instantiated
     * @throws NoSuchMethodException when a certain method cannot be found.
     * @throws InvocationTargetException is a checked exception that wraps an exception
     * thrown by a called method or constructor
     */
    public static void main(final String[] args) throws InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        ShapeFactory shapeFactory = new ShapeFactory();
        System.out.println(shapeFactory.getInstanceOf(Rectangle.class, "Rectangle"));
        System.out.println(shapeFactory.getInstanceOf(Circle.class, "Circle"));
        System.out.println("Checking the equality of objects: " + EqualsObject.equalObjects(new Rectangle("rectangle"), new Rectangle("rectangle", 5, 6)));
    }

    private Main() {

    }
}
