package by.epam.annotations;

import by.epam.impl.EqualsObject;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
/**
 *Annotation for fields of object
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Equal {
    /**
     *Attribute setting the comparison value field
     * @return type of comparison
     */
   EqualsObject.Compareby type();
}
