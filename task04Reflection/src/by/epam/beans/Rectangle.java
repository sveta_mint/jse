package by.epam.beans;

import by.epam.impl.EqualsObject;
import by.epam.impl.InvocationHandlerImpl;
import by.epam.annotations.Equal;
import by.epam.annotations.Proxy;

import java.util.Objects;

/**
 * Class describing the rectangle
 */
@Proxy(invocationHandler = InvocationHandlerImpl.class)
public class Rectangle extends Shape {
    @Equal(type = EqualsObject.Compareby.VALUE)
    private String name;
    @Equal(type = EqualsObject.Compareby.REFERENCE)
    private int sideA;
    @Equal(type = EqualsObject.Compareby.REFERENCE)
    private int sideB;

    /**
     * constructor to create a shape's object by name
     * @param name shape's
     */
    public Rectangle(final String name) {
        this.name = name;
    }

    /**
     * constructor to create a shape's object by name and sides
     * @param name shape's
     * @param sideA side rectangle
     * @param sideB side rectangle
     */
    public Rectangle(final String name, final int sideA, final int sideB) {
        this.name = name;
        this.sideA = sideA;
        this.sideB = sideB;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Getter for side rectangle
     * @return side of rectangle
     */
    public int getSideA() {
        return sideA;
    }

    /**
     * Setter for side rectangle
     * @param sideA side of rectangle
     */
    public void setSideA(final int sideA) {
        this.sideA = sideA;
    }

    /**
     * Getter for side rectangle
     * @return side of rectangle
     */
    public int getSideB() {
        return sideB;
    }

    /**
     * Setter for side rectangle
     * @param sideB side of rectangle
     */
    public void setSideB(final int sideB) {
        this.sideB = sideB;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rectangle rectangle = (Rectangle) o;
        return getSideA() == rectangle.getSideA() &&
                getSideB() == rectangle.getSideB() &&
                Objects.equals(getName(), rectangle.getName());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getName(), getSideA(), getSideB());
    }

    @Override
    public String toString() {
        return name;
    }
}
