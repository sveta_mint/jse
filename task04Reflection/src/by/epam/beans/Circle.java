package by.epam.beans;

import java.util.Objects;

/**
 * Class describing the circle
 */
public class Circle extends Shape {
    private String name;

    /**
     * constructor to create a shape's object by name
     * @param name shape's
     */
    public Circle(final String name) {
        this.name = name;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Circle circle = (Circle) o;
        return Objects.equals(getName(), circle.getName());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getName());
    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public void setName(final String name) {
        this.name = name;
    }
}
