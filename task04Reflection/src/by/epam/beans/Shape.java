package by.epam.beans;

import by.epam.impl.IShape;

/**
 * Class describing a geometric figure
 */
public abstract class Shape implements IShape {
    private String name;

    @Override
    public String getName() {
        return name;
    }

    /**
     * Setter for name shape
     * @param name shape's
     */
    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
