package by.epam;

import org.apache.tools.ant.*;
import java.io.File;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Validator for build file
 */
public class BuildFileValidator extends Task {
    private static final Pattern PATTERN = Pattern.compile("^[a-zA-Z][a-zA-Z-]+[a-zA-Z]$");
    private static final String MAINTARGER = "main";
    private boolean checkDepends;
    private boolean checkDefaults;
    private boolean checkNames;
    private List<BuildFile> buildFiles = new ArrayList<>();

    /**
    * Create nested element in custom task
    * @return buildfileElement
    */
    public BuildFile createBuildFile() {
        BuildFile buildFile = new BuildFile();
        buildFiles.add(buildFile);
        return buildFile;
    }

    private Project initProject(final BuildFile buildFile) {
        final Project project = new Project();
        project.init();
        ProjectHelper.configureProject(project, new File(buildFile.getLocation()));
        return project;
    }

    @Override
    public void execute() throws BuildException {
        for (BuildFile buildFile : buildFiles) {
            Project project = initProject(buildFile);

            if (checkDefaults) {
                isExistDefaultAttr(project);
            }

            if (checkNames) {
                isValidTagretNames(project);
            }

            if (checkDepends) {
                isIncludeDepends(project);
            }
        }
    }

    private void isValidTagretNames(final Project project) {
        for (String nameTarget : project.getTargets().keySet()) {
            if (!(PATTERN.matcher(nameTarget).find() || "".equals(nameTarget))) {
                throw new BuildException("Wrong target name!");
            }
        }
    }

    private void isExistDefaultAttr(final Project project) {
        if (project.getDefaultTarget() == null) {
            throw new BuildException("Default attribute dosen't exist!");
        }

    }

    private void isIncludeDepends(final Project project) {
        if (Collections.list(project.getTargets().get(MAINTARGER).getDependencies()).isEmpty()) {
            throw new BuildException("Main targets doesn't exist dependens!");
        }
        Set<String> otherDependens = project.getTargets().keySet();
        otherDependens.remove(MAINTARGER);
        Iterator<String> iterator = otherDependens.iterator();
        while (iterator.hasNext()) {
           if (!Collections.list(project.getTargets().get(iterator.next()).getDependencies()).isEmpty()) {
                throw new BuildException("Only main target can have dependencies!");
           }
        }
    }
    /**
     * Setteer for attribute custom task
     * @param  checkDepends default target.*/
    public void setCheckdepends(final boolean checkDepends) {
        this.checkDepends = checkDepends;
    }

    /**
     * Setteer for attribute custom task
     * @param  checkDefaults  target
     */
    public void setCheckdefaults(final boolean checkDefaults) {
        this.checkDefaults = checkDefaults;
    }

    /**
     * Setteer for attribute custom task
     * @param  checkNames for all target
     */
    public void setChecknames(final boolean checkNames) {
        this.checkNames = checkNames;
    }

    /**
     * Nested class for element custom task - buildfile
     */
    public static class BuildFile {
        private String location;
        BuildFile() {

        }
        /**
         * setter for
         * @param  location file
         */
        public void setLocation(final String location) {
            this.location = location;
        }

        /**
         * get location xml-file
         * @return location path
         */
        public String getLocation() {
            return location;
        }
    }
}
