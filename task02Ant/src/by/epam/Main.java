package by.epam;

/**
 * Main class
 */
public final class Main {

    /**
     * Default constructor
     */
    private Main() {

    }

    /**
     * Entry point to the programm
     * @param args
     * arguments of method
     */
    public static void main(final String[] args) {
        System.out.println("Hello!");
    }
}
