package by.epam.facade;

import by.epam.dto.ProductDto;
import by.epam.model.Category;
import by.epam.model.Currency;
import by.epam.model.Price;
import by.epam.model.Product;
import by.epam.repository.CategoryDAO;
import by.epam.repository.PriceDAO;
import by.epam.repository.ProductDAO;
import by.epam.utils.ConverterCategoryImpl;
import by.epam.utils.ConverterPriceImpl;
import by.epam.utils.ConverterProductImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductFacadeTest {
    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private ProductDAO productDAO;
    @Autowired
    private PriceDAO priceDAO;
    @Autowired
    private CategoryDAO categoryDAO;
    private Pageable pageable;
    private Product product;
    private Price price;
    private Category category;
    private ConverterProductImpl converterProduct;
    private ConverterPriceImpl converterPrice;
    private ProductFacade productFacade;
    private ProductDto productDto;

    @Before
    public void setUp() throws Exception {
        converterProduct = new ConverterProductImpl(priceDAO, categoryDAO);
        converterPrice = new ConverterPriceImpl();
        converterCategory = new ConverterCategoryImpl(categoryDAO);
        productFacade = new ProductFacade(productDAO, converterProduct, categoryDAO, priceDAO);
        priceFacade = new PriceFacade(priceDAO, converterPrice, productDAO);
        pageable = PageRequest.of(0, 10, Sort.Direction.ASC, "name");
        price = entityManager.persistAndFlush(new Price(100, Currency.USD));
        category = entityManager.persistAndFlush(new Category("category1", null));
        product = entityManager.persistAndFlush(new Product("product1", Arrays.asList(price),Arrays.asList(category)));
        productDto = converterProduct.convertModelToDto(product);
    }

    @After
    public void tearDown() throws Exception {
        entityManager.clear();
    }

    @Test
    public void getProductsByNameSuccessfully() {
        List<ProductDto> productsDto = productFacade.getProductsByName("product1", pageable);
        boolean result = productsDto
                .stream()
                .allMatch(p-> p.getName() == "product1");
        assertTrue(result);
    }

    @Test
    public void getProductsEmptyListByWrongName() {
        List<ProductDto> productsDto = productFacade.getProductsByName("product", pageable);
        assertTrue(productsDto.isEmpty());
    }

    @Test
    public void getProductsByPriceSuccessfully() {
        List<ProductDto> products = productFacade.getProductsByPrice(price.getId(),pageable);
        boolean result = products
                        .stream()
                        .allMatch(p -> p.getPricesId().contains(price.getId()));
        assertTrue(result);
    }

    @Test
    public void getEmptyListProductsByWrongPriceValue() {
        Price wrongPrice = entityManager.persistAndFlush(new Price(50, Currency.USD));
        List<ProductDto> productsByPrice = productFacade.getProductsByPrice(wrongPrice.getId(), pageable);
        assertTrue(productsByPrice.isEmpty());

    }

    @Test
    public void getEmptyListProductsByWrongCurrency() {
        Price wrongPrice = entityManager.persistAndFlush(new Price(100, Currency.RUB));
        List<ProductDto> productsByPrice = productFacade.getProductsByPrice(wrongPrice.getId(), pageable);
        assertTrue(productsByPrice.isEmpty());
    }

    @Test
    public void getEmptyListProductsByWrongPrice() {
        Price wrongPrice = entityManager.persistAndFlush(new Price(50, Currency.RUB));
        List<ProductDto> productsByPrice = productFacade.getProductsByPrice(wrongPrice.getId(), pageable);
        assertTrue(productsByPrice.isEmpty());
    }

    @Test
    public void getProductsByIdCategorySuccessfully() {
       List<ProductDto> productsDto = productFacade.getProductsByIdCategory(category.getId(), pageable);
       assertEquals(1,productsDto.size());
    }

    @Test
    public void getNullProductsByWrongIdCategory() {
        List<ProductDto> productsDto = productFacade.getProductsByIdCategory(category.getId()+100, pageable);
        assertNull(productsDto);
    }

    @Test
    public void getAllModelsSuccessfully() {
        assertEquals(1, productFacade.getAllModels(pageable).size());
    }

    @Test
    public void getAllModelsByWrongSize() {
        assertNotEquals(5, productFacade.getAllModels(pageable).size());
    }

    @Test
    public void getModelByIdSuccessfully() {
        assertEquals(productDto, productFacade.getModelById(productDto.getId()));
    }

    @Test
    public void getNullFromModelByWrongId() {
        productDto.setId(5L);
        assertNull(productFacade.getModelById(productDto.getId()));
    }

    @Test
    public void deleteModelSuccessfully() {
        int count = productFacade.getAllModels(pageable).size();
        productFacade.deleteModel(productDto.getId());
        assertEquals(count-1, productFacade.getAllModels(pageable).size());
    }

    @Test
    public void deleteModelByNotExistId() {
        int count = productFacade.getAllModels(pageable).size();
        productDto.setId(200L);
        productFacade.deleteModel(productDto.getId());
        assertEquals(count, productFacade.getAllModels(pageable).size());
    }

    @Test
    public void createModelSuccessfully() {
        Product saveProduct = new Product("qwerty", Arrays.asList(new Price(19, Currency.BYN)),Arrays.asList(new Category("asdasd", null)));
        ProductDto saveProductDto = productFacade.createModel(converterProduct.convertModelToDto(saveProduct));
        saveProduct.setId(saveProductDto.getId());
        saveProduct.setName(saveProductDto.getName());
        saveProduct.setPrices(priceDAO.findAllById(saveProductDto.getPricesId()));
        saveProduct.setCategories(categoryDAO.findAllById(saveProductDto.getCategoriesId()));
        assertEquals(converterProduct.convertModelToDto(saveProduct), saveProductDto);
    }

    @Test(expected = org.springframework.dao.DataIntegrityViolationException.class)
    public void createModelFail() {
        Product saveProduct = new Product();
        ProductDto saveProductDto = productFacade.createModel(converterProduct.convertModelToDto(saveProduct));
        saveProduct.setId(saveProductDto.getId());
    }

    @Test(expected = org.springframework.dao.InvalidDataAccessApiUsageException.class)
    public void editModelFailByNotExistPrice() {
        productDto.setId(null);
        productDto.setName(null);
        productDto.setCategoriesId(null);
        productDto.setCategoriesId(null);
        productFacade.editModel(productDto);
    }

    @Test
    public void editModelReturnNullByNotExistId() {
        productDto.setId(200L);
        productDto.setName("fghjk");
        ProductDto editProductDto = productFacade.editModel(productDto);
        assertNull(editProductDto);
    }
}