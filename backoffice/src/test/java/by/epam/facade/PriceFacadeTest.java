package by.epam.facade;

import by.epam.dto.PriceDto;
import by.epam.model.Category;
import by.epam.model.Currency;
import by.epam.model.Price;
import by.epam.model.Product;
import by.epam.repository.PriceDAO;
import by.epam.repository.ProductDAO;
import by.epam.utils.ConverterPriceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PriceFacadeTest {
    @Autowired
    private TestEntityManager entityManager;
    private Pageable pageable;
    @Autowired
    private PriceDAO priceDAO;
    @Autowired
    private ProductDAO productDAO;
    private PriceDto priceDto;
    private PriceFacade priceFacade;
    private ConverterPriceImpl converterPrice;
    private Price price;

    @Before
    public void setUp() throws Exception {
        pageable = PageRequest.of(0, 1, Sort.Direction.ASC, "value");
        converterPrice =  new ConverterPriceImpl();
        price = new Price(100, Currency.BYN);
        priceFacade = new PriceFacade(priceDAO, converterPrice, productDAO);
        priceDto = converterPrice.convertModelToDto(entityManager.persistAndFlush(price));

    }

    @After
    public void tearDown() throws Exception {
        entityManager.clear();
    }

    @Test
    public void getPricesByCurrencySuccessfully() {
        List<PriceDto> prices = priceFacade.getPricesByCurrency(Currency.BYN, pageable);
        boolean result = prices
                .stream()
                .allMatch(p -> p.getCurrency() == Currency.BYN);
        assertTrue(result);
    }

    @Test
    public void getPricesByWrongCurrency() {
        List<PriceDto> prices = priceFacade.getPricesByCurrency(Currency.USD, pageable);
        assertTrue(prices.isEmpty());
    }

    @Test
    public void getPriceByProductSuccessfully() {
        Category category = entityManager.persistAndFlush(new Category("category", null));
        Product product = entityManager.persistAndFlush(new Product("apple", Arrays.asList(price), Arrays.asList(category)));
        List<PriceDto> prices = priceFacade.getPriceByProduct(product.getId());
        assertArrayEquals(prices.toArray(), converterPrice.getListConvertModelDto(product.getPrices()).toArray());
    }

    @Test
    public void getEmptyPriceListByWrongIdProduct() {
        List<PriceDto> prices = priceFacade.getPriceByProduct(5L);
        assertTrue(prices.isEmpty());
    }

    @Test
    public void getPriceByValueBetweenAndCurrencyShouldReturnTrue() {
        List<PriceDto> prices = priceFacade.getPriceByRange(50, 200, Currency.BYN, pageable);
        boolean result = prices
                .stream()
                .allMatch(p->p.getCurrency() == Currency.BYN && p.getValue()>=50 && p.getValue()<=200);
        assertTrue(result);
    }

    @Test
    public void getEmptyPriceListByRangeAndWrongCurrency() {
        List<PriceDto> prices = priceFacade.getPriceByRange(50, 200, Currency.USD, pageable);
        assertTrue(prices.isEmpty());
    }

    @Test
    public void getEmptyPriceListByWrongRangeAndCurrency() {
        List<PriceDto> prices = priceFacade.getPriceByRange(150, 200, Currency.BYN, pageable);
        assertTrue(prices.isEmpty());
    }

    @Test
    public void getAllModelsSuccessfully() {
        assertEquals(1, priceFacade.getAllModels(pageable).size());
    }

    @Test
    public void getAllModelsByWrongSize() {
        assertNotEquals(2, priceFacade.getAllModels(pageable).size());
    }

    @Test
    public void getModelByIdSuccessfully() {
        assertEquals(priceDto, priceFacade.getModelById(priceDto.getId()));
    }

    @Test
    public void getNullFromModelByWrongId() {
        priceDto.setId(5L);
        assertNull(priceFacade.getModelById(priceDto.getId()));
    }

    @Test
    public void deleteModelSuccessfully() {
        int count = priceFacade.getAllModels(pageable).size();
        priceFacade.deleteModel(priceDto.getId());
        assertEquals(count-1, priceFacade.getAllModels(pageable).size());
    }

    @Test
    public void deleteModelByNotExistId() {
        int count = priceFacade.getAllModels(pageable).size();
        priceDto.setId(200L);
        priceFacade.deleteModel(priceDto.getId());
        assertEquals(count, priceFacade.getAllModels(pageable).size());
    }

    @Test
    public void createModelSuccessfully() {
        Price savePrice = new Price(60, Currency.BYN);
        PriceDto savePriceDto = priceFacade.createModel(converterPrice.convertModelToDto(savePrice));
        savePrice.setId(savePriceDto.getId());
        assertEquals(converterPrice.convertModelToDto(savePrice), savePriceDto);
    }

    @Test(expected = org.springframework.dao.DataIntegrityViolationException.class)
    public void createModelFail() {
        Price savePrice = new Price();
        PriceDto savePriceDto = priceFacade.createModel(converterPrice.convertModelToDto(savePrice));
        savePrice.setId(savePriceDto.getId());
    }

    @Test
    public void editModelSuccessfully() {
        priceDto.setValue(50);
        PriceDto editPriceDto = priceFacade.editModel(priceDto);
        PriceDto expectedPriceDto = new PriceDto();
        expectedPriceDto.setId(priceDto.getId());
        expectedPriceDto.setValue(editPriceDto.getValue());
        expectedPriceDto.setCurrency(editPriceDto.getCurrency());
        assertEquals(expectedPriceDto, editPriceDto);
    }

    @Test(expected = org.springframework.dao.InvalidDataAccessApiUsageException.class)
    public void editModelFailByNotExistPrice() {
        priceDto.setId(null);
        priceDto.setValue(null);
        priceDto.setCurrency(null);
        priceFacade.editModel(priceDto);
    }

    @Test
    public void editModelReturnNullByNotExistId() {
        priceDto.setId(200L);
        priceDto.setValue(50);
        PriceDto editPriceDto = priceFacade.editModel(priceDto);
        assertNull(editPriceDto);
    }

    @Test
    public void editModelCountPricesNotChanged() {
        int count = priceFacade.getAllModels(pageable).size();
        priceFacade.editModel(priceDto);
        assertEquals(count, priceFacade.getAllModels(pageable).size());
    }

}