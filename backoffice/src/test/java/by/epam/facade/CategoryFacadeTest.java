package by.epam.facade;

import by.epam.dto.CategoryDto;
import by.epam.dto.PriceDto;
import by.epam.model.Category;
import by.epam.repository.CategoryDAO;
import by.epam.repository.PriceDAO;
import by.epam.repository.ProductDAO;
import by.epam.utils.ConverterCategoryImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CategoryFacadeTest {
    @Autowired
    private TestEntityManager entityManager;
    private Pageable pageable;
    @Autowired
    private ProductDAO productDAO;
    @Autowired
    private CategoryDAO categoryDAO;
    private ConverterCategoryImpl converterCategory;
    private CategoryDto categoryDto;
    private CategoryFacade categoryFacade;
    private Category parentCategory,subCategory;

    @Before
    public void setUp() throws Exception {
        converterCategory = new ConverterCategoryImpl(categoryDAO);
        categoryFacade = new CategoryFacade(categoryDAO, converterCategory, productDAO);
        pageable = PageRequest.of(0, 10, Sort.Direction.ASC, "name");
        parentCategory = entityManager.persistAndFlush(new Category("category0",null));
        subCategory = entityManager.persistAndFlush(new Category("category1", parentCategory));
        categoryDto = converterCategory.convertModelToDto(parentCategory);
    }

    @After
    public void tearDown() throws Exception {
        entityManager.clear();
    }

    @Test
    public void getCategoriesByNameSuccessfully() {
        List<CategoryDto> categoryDto = categoryFacade.getCategoriesByName("caterogy0", pageable);
        boolean result = categoryDto
                .stream()
                .allMatch(c ->  c.getName() == "category0");
        assertTrue(result);
    }

    @Test
    public void getCategoriesByWrongName() {
        List<CategoryDto> categoryDto = categoryFacade.getCategoriesByName("caterogy2", pageable);
        assertTrue(categoryDto.isEmpty());
    }

    @Test
    public void getAllModelsSuccessfully() {
        assertEquals(2, categoryFacade.getAllModels(pageable).size());
    }

    @Test
    public void getAllModelsByWrongSize() {
        assertNotEquals(5, categoryFacade.getAllModels(pageable).size());
    }

    @Test
    public void getModelByIdSuccessfully() {
        assertEquals(categoryDto, categoryFacade.getModelById(categoryDto.getId()));
    }

    @Test
    public void getNullFromModelByWrongId() {
        categoryDto.setId(5L);
        assertNull(categoryFacade.getModelById(categoryDto.getId()));
    }

    @Test
    public void deleteModelSuccessfully() {
        int count = categoryFacade.getAllModels(pageable).size();
        CategoryDto subCategoryDto = converterCategory.convertModelToDto(subCategory);
        categoryFacade.deleteModel(subCategoryDto.getId());
        assertEquals(count-1, categoryFacade.getAllModels(pageable).size());
    }

    @Test
    public void deleteModelByNotExistId() {
        int count = categoryFacade.getAllModels(pageable).size();
        categoryDto.setId(200L);
        categoryFacade.deleteModel(categoryDto.getId());
        assertEquals(count, categoryFacade.getAllModels(pageable).size());
    }

    @Test
    public void editModelSuccessfully() {
        categoryDto.setName("category");
        CategoryDto editCategoryDto = categoryFacade.editModel(categoryDto);
        CategoryDto expectedCategoryDto = new CategoryDto();
        expectedCategoryDto.setId(categoryDto.getId());
        expectedCategoryDto.setName(editCategoryDto.getName());
        expectedCategoryDto.setIdParentCategory(editCategoryDto.getIdParentCategory());
        assertEquals(expectedCategoryDto, editCategoryDto);
    }

    @Test(expected = org.springframework.dao.InvalidDataAccessApiUsageException.class)
    public void editModelFailByNotExistCategory() {
        categoryDto.setId(null);
        categoryDto.setName(null);
        categoryDto.setIdParentCategory(null);
        categoryFacade.editModel(categoryDto);
    }

    @Test
    public void editModelReturnNullByNotExistId() {
        categoryDto.setId(200L);
        categoryDto.setName("qwerty");
        categoryDto.setIdParentCategory(null);
        CategoryDto editCategoryDto = categoryFacade.editModel(categoryDto);
        assertNull(editCategoryDto);
    }

    @Test
    public void editModelCountPricesNotChanged() {
        int count = categoryFacade.getAllModels(pageable).size();
        categoryFacade.editModel(categoryDto);
        assertEquals(count, categoryFacade.getAllModels(pageable).size());
    }


}