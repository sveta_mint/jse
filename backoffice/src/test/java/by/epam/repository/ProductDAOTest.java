package by.epam.repository;

import by.epam.model.Category;
import by.epam.model.Currency;
import by.epam.model.Price;
import by.epam.model.Product;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductDAOTest {
    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private ProductDAO productDAO;
    private Pageable pageable;
    private Product product;
    private Price price;
    private Category category;

    @Before
    public void setUp() throws Exception {
        pageable = PageRequest.of(0, 1, Sort.Direction.ASC, "name");
        price = entityManager.persistAndFlush(new Price(100, Currency.USD));
        category = entityManager.persistAndFlush(new Category("category1", null));
        product = entityManager.persistAndFlush(new Product("product1", Arrays.asList(price),Arrays.asList(category)));
    }

    @After
    public void tearDown() throws Exception {
        entityManager.clear();
    }

    @Test
    public void findProductsByNameShouldReturnTrue() {
        List<Product> products = productDAO.findProductsByName("product1", pageable);
        boolean result = products
                .stream()
                .allMatch(p-> p.getName() == "product1");
        assertTrue(result);
    }

    @Test
    public void findProductsByWrongNameShouldReturnEmptyList() {
        List<Product> products = productDAO.findProductsByName("product0", pageable);
        assertTrue(products.isEmpty());
    }

    @Test
    public void findProductsByPricesShouldReturnTrue() {
        List<Product> products = productDAO.findProductsByPrices(price, pageable);
        boolean result = products
                .stream()
                .allMatch(p-> p.getPrices().contains(price));
        assertTrue(result);
    }

    @Test
    public void findProductsByWrongPricesShouldReturnEmptyList() {
        Price wrongPrice = entityManager.persistAndFlush(new Price(100, Currency.BYN));
        List<Product> products = productDAO.findProductsByPrices(wrongPrice, pageable);
        assertTrue(products.isEmpty());
    }

    @Test
    public void findProductsByCategoriesShouldReturnTrue() {
        List<Product> products = productDAO.findProductsByCategories(category, pageable);
        boolean result = products
                .stream()
                .allMatch(p->p.getCategories().contains(category));
        assertTrue(result);
    }
    @Test
    public void findProductsByWrongCategoriesShouldReturnEmptyList() {
        Category wrongCategory = entityManager.persistAndFlush(new Category("category0",null));
        List<Product> products = productDAO.findProductsByCategories(wrongCategory, pageable);
        assertTrue(products.isEmpty());
    }
}