package by.epam.repository;

import by.epam.model.Category;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;
@RunWith(SpringRunner.class)
@DataJpaTest
public class CategoryDAOTest {
    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private CategoryDAO categoryDAO;
    private Category category, subCategory;
    private Pageable page;

    @Before
    public void setUp() throws Exception {
        page = PageRequest.of(0, 1, Sort.Direction.ASC, "name");
        category = entityManager.persistAndFlush(new Category("category0",null));
        subCategory = entityManager.persistAndFlush(new Category("category1", category));
    }

    @After
    public void tearDown() throws Exception {
        entityManager.clear();
    }

    @Test
    public void findCategoryByNameShouldReturnTrue() {
        List<Category> categories = categoryDAO.findCategoryByName("category0", page);
        boolean result = categories
                .stream()
                .allMatch(c-> c.getName() == "category0");
        assertTrue(result);
    }

    @Test
    public void findCategoryByWrongNameReturnEmptyList() {
        List<Category> categories = categoryDAO.findCategoryByName("category2", page);
        assertTrue(categories.isEmpty());
    }

    @Test
    public void findCategoryByNameSubCategoryShouldReturnTrue() {
        List<Category> categories = categoryDAO.findCategoryByName("category1", page);
        boolean result = categories
                .stream()
                .allMatch(c-> c.getParentCategory().getName() == "category0");
        assertTrue(result);
    }

    @Test
    public void findCategoryByWrongNameSubCategoryShouldReturnEmptyList() {
        List<Category> subCategories = categoryDAO.findCategoryByName("category2", page);
        assertTrue(subCategories.isEmpty());
    }

}