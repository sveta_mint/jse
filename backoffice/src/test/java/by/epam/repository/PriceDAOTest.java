package by.epam.repository;

import by.epam.model.Currency;
import by.epam.model.Price;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PriceDAOTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private PriceDAO priceDAO;
    private Pageable page;
    private Price price;

    @Before
    public void setUp() {
        page = PageRequest.of(0, 1, Sort.Direction.ASC, "value");
        price = entityManager.persistAndFlush(new Price(100, Currency.USD));
    }

    @After
    public void tearDown() throws Exception {
        entityManager.clear();
    }

    @Test
    public void findPricesByCurrencyShouldReturnPriceWithUSDCurrency() {
        List<Price> prices = priceDAO.findPricesByCurrency(Currency.USD, page);
        boolean result = prices
                .stream()
                .allMatch(p-> p.getCurrency()== Currency.USD);
        assertTrue(result);
    }

    @Test
    public void findPricesByCurrencyShouldReturnEmptyListByWrongCurrency() {
        List<Price> prices = priceDAO.findPricesByCurrency(Currency.BYN, page);
        assertTrue(prices.isEmpty());
    }

    @Test
    public void findPricesByValueBetweenAndCurrencyShouldReturnTrue() {
        List<Price> prices = priceDAO.findPricesByValueBetweenAndCurrency(50, 200, Currency.USD, page);
        boolean result = prices
                .stream()
                .allMatch(p->p.getCurrency() == Currency.USD && p.getValue()>=50 && p.getValue()<=200);
        assertTrue(result);
    }

    @Test
    public void findPricesShouldReturnEmptyListByWrongRange() {
        List<Price> prices = priceDAO.findPricesByValueBetweenAndCurrency(150, 200, Currency.USD, page);
        assertTrue(prices.isEmpty());
    }

    @Test
    public void findPricesShouldReturnEmptyListByWrongCurrency() {
        List<Price> prices = priceDAO.findPricesByValueBetweenAndCurrency(50, 200, Currency.BYN, page);
        assertTrue(prices.isEmpty());
    }
}