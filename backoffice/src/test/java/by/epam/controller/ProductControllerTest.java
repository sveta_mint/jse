package by.epam.controller;

import by.epam.dto.CategoryDto;
import by.epam.dto.PriceDto;
import by.epam.dto.ProductDto;
import by.epam.facade.PriceFacade;
import by.epam.facade.ProductFacade;
import by.epam.model.Category;
import by.epam.model.Currency;
import by.epam.model.Price;
import by.epam.model.Product;
import by.epam.repository.PriceDAO;
import by.epam.repository.ProductDAO;
import by.epam.utils.ConverterCategoryImpl;
import by.epam.utils.ConverterPriceImpl;
import by.epam.utils.ConverterProductImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ProductControllerTest {
    @Autowired
    private WebApplicationContext context;
    @Autowired
    private ProductFacade productFacade;
    @Autowired
    private ConverterPriceImpl converterPrice;
    @Autowired
    private ConverterCategoryImpl converterCategory;
    private MockMvc mvc;
    private ObjectMapper mapper;
    private Pageable pageable;
    private ProductDto productDto;

    @Before
    public void setUp() throws Exception {
        pageable = PageRequest.of(0, 20, Sort.Direction.ASC, "id");
        mapper = new Jackson2JsonEncoder().getObjectMapper();
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
        PriceDto priceDto = converterPrice.convertModelToDto(new Price(500, Currency.USD));
        priceDto.setId(1L);
        CategoryDto categoryDto = converterCategory.convertModelToDto(new Category("category", null));
        categoryDto.setId(1L);
        productDto =  new ProductDto();
        productDto.setId(1L);
        productDto.setName("product");
        productDto.setPricesId(Arrays.asList(priceDto.getId()));
        productDto.setCategoriesId(Arrays.asList(categoryDto.getId()));
    }

    @After
    public void tearDown() throws Exception {
        mapper = null;
        mvc = null;
    }

    @Test
    @WithMockUser(username="user",password = "user",roles="USER")
    public void getProductByNameByUser() throws Exception {
        String expected = mapper.writeValueAsString(new ProductDto[] {});
        mvc.perform(get("/product/search/name/" + "product")
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(expected));

    }

    @Test
    @WithMockUser(username="user",password = "user",roles="USER")
    public void getProductByPriceByUser() throws Exception {
        String expected = mapper.writeValueAsString(new ProductDto[]{});
        mvc.perform(get("/product/search/price/"+ productDto.getPricesId().get(0))
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(expected));
    }

    @Test
    @WithMockUser(username="user",password = "user",roles="USER")
    public void getProductByIdCategory() throws Exception{
        String expected = mapper.writeValueAsString(new ProductDto[]{});
        mvc.perform(get("/product/search/category/"+ productDto.getCategoriesId().get(0))
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(expected));
    }

    @Test
    @WithMockUser(username="user",password = "user",roles="USER")
    public void deletePriceByUserIsForbidden() throws Exception{
        mvc.perform(delete("/product/delete/")
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username="admin",password = "admin",roles="ADMIN")
    public void deletePriceByAdminIsNoContent() throws Exception {
        productDto.setId(1L);
        mvc.perform(delete("/product/delete/" + productDto.getId())
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

    }

    @Test
    @WithMockUser(username="user",password = "user",roles="USER")
    public void addProductByUserIsForbidden() throws Exception{
        mvc.perform(post("/product/add")
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username="admin",password = "admin",roles="ADMIN")
    public void addProductByAdminIsCreated() throws Exception {
        ProductDto saveProductDto = productFacade.createModel(productDto);
        String expected = mapper.writeValueAsString(saveProductDto);
        mvc.perform(post("/product/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(expected))
                .andExpect(status().isCreated())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", is (productDto.getName())))
                .andExpect(jsonPath("$.pricesId", is(productDto.getPricesId()
                        .stream()
                        .map(Long::intValue)
                        .collect(Collectors.toList()))))
                .andExpect(jsonPath("$.categoriesId", is(productDto.getCategoriesId()
                        .stream()
                        .map(Long::intValue)
                        .collect(Collectors.toList()))));
    }

    @Test
    @WithMockUser(username="user",password = "user",roles="USER")
    public void editProductByUserIsForbidden() throws Exception{
        mvc.perform(put("/product/edit")
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username="admin",password = "admin",roles="ADMIN")
    public void editProductByAdminIsOk() throws Exception{
        productDto.setName("qweqwe");
        productFacade.editModel(productDto);
        String expected = mapper.writeValueAsString(productDto);
        mvc.perform(put("/product/edit")
                .contentType(MediaType.APPLICATION_JSON)
                .content(expected))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is (productDto.getId().intValue())))
                .andExpect(jsonPath("$.name", is (productDto.getName())))
                .andExpect(jsonPath("$.pricesId", is(productDto.getPricesId()
                        .stream()
                        .map(Long::intValue)
                        .collect(Collectors.toList()))))
                .andExpect(jsonPath("$.categoriesId", is(productDto.getCategoriesId()
                        .stream()
                        .map(Long::intValue)
                        .collect(Collectors.toList()))));

    }

    @Test
    @WithMockUser(username="user",password = "user",roles="USER")
    public void getProductByIdByUser() throws Exception {
        String expected = mapper.writeValueAsString(productDto);
        mvc.perform(get("/product/id/"+1)
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(expected));
    }

}