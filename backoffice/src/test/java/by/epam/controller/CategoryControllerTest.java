package by.epam.controller;

import by.epam.dto.CategoryDto;
import by.epam.facade.CategoryFacade;
import by.epam.model.Category;
import by.epam.utils.ConverterCategoryImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.*;


@SpringBootTest
@RunWith(SpringRunner.class)
public class CategoryControllerTest {
    @Autowired
    private WebApplicationContext context;

    @Autowired
    private CategoryFacade categoryFacade;
    @Autowired
    private ConverterCategoryImpl converterCategory;
    private MockMvc mvc;
    private ObjectMapper mapper;
    private Pageable pageable;

    @Before
    public void setUp() throws Exception {
        pageable = PageRequest.of(0, 20, Sort.Direction.ASC, "id");
        mapper = new Jackson2JsonEncoder().getObjectMapper();
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }


    @After
    public void tearDown() throws Exception {
        mapper = null;
        mvc = null;
    }

    @Test
    @WithMockUser(username="user",password = "user",roles="USER")
    public void getCategoriesByNameByUserIsOk() throws Exception{
        String expected = mapper.writeValueAsString(categoryFacade.getCategoriesByName("category1", pageable));
        mvc.perform(get("/category/search/name/"+ "category1")
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(expected));
    }

    @Test
    @WithMockUser(username="admin",password = "admin",roles="ADMIN")
    public void getCategoriesByNameByAdminIsOk() throws Exception{
        String expected = mapper.writeValueAsString(categoryFacade.getCategoriesByName("category1", pageable));
        mvc.perform(get("/category/search/name/"+ "category1")
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(expected));
    }

    @Test
    @WithMockUser(username="user",password = "user",roles="USER")
    public void findAllCategoriesSuccessfullyByUser() throws Exception {
        String expected = mapper.writeValueAsString(categoryFacade.getAllModels(pageable));
        mvc.perform(get("/category/all")
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(expected));
    }

    @Test
    @WithMockUser(username="admin",password = "admin",roles="ADMIN")
    public void findAllCategoriesSuccessfullyByAdmin() throws Exception {
        String expected = mapper.writeValueAsString(categoryFacade.getAllModels(pageable));
        mvc.perform(get("/category/all")
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(expected));
    }

    @Test
    @WithMockUser(username="user",password = "user",roles="USER")
    public void deleteCategoryByUserIsForbidden() throws Exception{
        mvc.perform(delete("/category/delete/")
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username="admin",password = "admin",roles="ADMIN")
    public void deleteCategoryByAdminIsNoContent() throws Exception {
        Category category = new Category("qwerty",null);
        CategoryDto categoryDto = categoryFacade.createModel(converterCategory.convertModelToDto(category));
        mvc.perform(delete("/category/delete/" + categoryDto.getId())
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    @WithMockUser(username="user",password = "user",roles="USER")
    public void addCategoryByUserIsForbidden() throws Exception{
        mvc.perform(post("/category/add")
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username="user",password = "user",roles="USER")
    public void editCategoryByUserIsForbidden() throws Exception{
        mvc.perform(put("/category/edit")
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username="admin",password = "admin",roles="ADMIN")
    public void editCategoryByAdminIsOk() throws Exception{
        CategoryDto categoryDto = categoryFacade.getModelById(1L);
        categoryDto.setName("qwerty");
        categoryFacade.editModel(categoryDto);
        String expected = mapper.writeValueAsString(categoryDto);
        mvc.perform(put("/category/edit")
                .contentType(MediaType.APPLICATION_JSON)
                .content(expected))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is (categoryDto.getId().intValue())))
                .andExpect(jsonPath("$.name", is (categoryDto.getName())))
                .andExpect(jsonPath("$.idParentCategory", is(categoryDto.getIdParentCategory())));

    }

    @Test
    @WithMockUser(username="admin",password = "admin",roles="ADMIN")
    public void addCategoryByAdminIsCreated() throws Exception {
        Category category = new Category("qwerty", null);
        CategoryDto categoryDto = categoryFacade.createModel(converterCategory.convertModelToDto(category));
        String expected = mapper.writeValueAsString(categoryDto);
        mvc.perform(post("/category/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(expected))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is (categoryDto.getId().intValue())))
                .andExpect(jsonPath("$.name", is (categoryDto.getName())))
                .andExpect(jsonPath("$.idParentCategory", is(categoryDto.getIdParentCategory())));
    }

    @Test
    @WithMockUser(username="user",password = "user",roles="USER")
    public void getCategoryByIdByUser() throws Exception {
        String expected = mapper.writeValueAsString(categoryFacade.getModelById(1L));
        mvc.perform(get("/category/id/"+1)
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(expected));
    }

    @Test
    @WithMockUser(username="admin",password = "admin",roles="ADMIN")
    public void getCategoryByIdByAdmin() throws Exception {
        String expected = mapper.writeValueAsString(categoryFacade.getModelById(1L));
        mvc.perform(get("/category/id/"+1)
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(expected));
    }
}