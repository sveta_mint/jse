package by.epam.controller;

import by.epam.dto.PriceDto;
import by.epam.dto.ProductDto;
import by.epam.facade.PriceFacade;
import by.epam.model.Currency;
import by.epam.model.Price;
import by.epam.utils.ConverterPriceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;



import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
public class PriceControllerTest {
    @Autowired
    private WebApplicationContext context;
    @Autowired
    private PriceFacade priceFacade;
    @Autowired
    private ConverterPriceImpl converterPrice;
    private MockMvc mvc;
    private ObjectMapper mapper;
    private Pageable pageable;

    @Before
    public void setUp() throws Exception {
        pageable = PageRequest.of(0, 20, Sort.Direction.ASC, "id");
        mapper = new Jackson2JsonEncoder().getObjectMapper();
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @After
    public void tearDown() throws Exception {
        mapper = null;
        mvc = null;
    }

    @Test
    @WithMockUser(username="user",password = "user",roles="USER")
    public void findPricesByCurrencyByUser() throws Exception{
        String expected = mapper.writeValueAsString(priceFacade.getPricesByCurrency(Currency.BYN, pageable));
        mvc.perform(get("/price/search/currency")
                .param("currency", Currency.BYN.toString())
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(expected));
    }

    @Test
    @WithMockUser(username="admin",password = "admin",roles="ADMIN")
    public void findPricesByCurrencyByAdmin() throws Exception{
        String expected = mapper.writeValueAsString(priceFacade.getPricesByCurrency(Currency.BYN, pageable));
        mvc.perform(get("/price/search/currency")
                .param("currency", Currency.BYN.toString())
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(expected));
    }


    @Test
    @WithMockUser(username="user",password = "user",roles="USER")
    public void getPricesByRangeByUser() throws Exception {
        String expected = mapper.writeValueAsString(priceFacade.getPriceByRange(50, 300, Currency.USD, pageable));
        mvc.perform(get("/price/search/range")
                .param("minAmount", "50")
                .param("maxAmount", "300")
                .param("currency", Currency.USD.toString())
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(expected));
    }

    @Test
    @WithMockUser(username="admin",password = "admin",roles="ADMIN")
    public void getPricesByRangeByAdmin() throws Exception {
        String expected = mapper.writeValueAsString(priceFacade.getPriceByRange(50, 300, Currency.BYN, pageable));
        mvc.perform(get("/price/search/range")
                .param("minAmount", "50")
                .param("maxAmount", "300")
                .param("currency", Currency.BYN.toString())
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(expected));
    }

    @Test
    @WithMockUser(username="user",password = "user",roles="USER")
    public void findAllPricesSuccessfullyByUser() throws Exception {
        String expected = mapper.writeValueAsString(priceFacade.getAllModels(pageable));
        mvc.perform(get("/price/all")
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(expected));
    }

    @Test
    @WithMockUser(username="admin",password = "admin",roles="ADMIN")
    public void findAllPricesSuccessfullyByAdmin() throws Exception {
        String expected = mapper.writeValueAsString(priceFacade.getAllModels(pageable));
        mvc.perform(get("/price/all")
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(expected));
    }


    @Test
    @WithMockUser(username="user",password = "user",roles="USER")
    public void deletePriceByUserIsForbidden() throws Exception{
        mvc.perform(delete("/price/delete/")
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username="admin",password = "admin",roles="ADMIN")
    public void deletePriceByAdminIsNoContent() throws Exception {
        Price price = new Price(55, Currency.RUB);
        PriceDto priceDto = priceFacade.createModel(converterPrice.convertModelToDto(price));
        mvc.perform(delete("/category/delete/" + priceDto.getId())
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    @WithMockUser(username="user",password = "user",roles="USER")
    public void addPriceByUserIsForbidden() throws Exception{
        mvc.perform(post("/price/add")
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username="admin",password = "admin",roles="ADMIN")
    public void addPriceByAdminIsCreated() throws Exception {
        Price price = new Price(555, Currency.RUB);
        PriceDto priceDto = priceFacade.createModel(converterPrice.convertModelToDto(price));
        String expected = mapper.writeValueAsString(priceDto);
        mvc.perform(post("/price/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(expected))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is (priceDto.getId().intValue())))
                .andExpect(jsonPath("$.value", is (priceDto.getValue())))
                .andExpect(jsonPath("$.currency", is(priceDto.getCurrency().toString())));
    }


    @Test
    @WithMockUser(username="user",password = "user",roles="USER")
    public void editPriceByUserIsForbidden() throws Exception{
        mvc.perform(put("/price/edit")
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username="admin",password = "admin",roles="ADMIN")
    public void editPriceByAdminIsOk() throws Exception{
        PriceDto priceDto = priceFacade.getModelById(1L);
        priceDto.setValue(6505);
        priceFacade.editModel(priceDto);
        String expected = mapper.writeValueAsString(priceDto);
        mvc.perform(put("/price/edit")
                .contentType(MediaType.APPLICATION_JSON)
                .content(expected))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is (priceDto.getId().intValue())))
                .andExpect(jsonPath("$.value", is (priceDto.getValue())))
                .andExpect(jsonPath("$.currency", is(priceDto.getCurrency().toString())));

    }

    @Test
    @WithMockUser(username="user",password = "user",roles="USER")
    public void getPriceByIdByUser() throws Exception {
        String expected = mapper.writeValueAsString(priceFacade.getModelById(1L));
        mvc.perform(get("/price/id/"+1)
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(expected));
    }

    @Test
    @WithMockUser(username="admin",password = "admin",roles="ADMIN")
    public void getPriceByIdByAdmin() throws Exception {
        String expected = mapper.writeValueAsString(priceFacade.getModelById(1L));
        mvc.perform(get("/price/id/"+1)
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(expected));
    }
}