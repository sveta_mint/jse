package by.epam.utils;

import by.epam.dto.CategoryDto;
import by.epam.model.Category;
import by.epam.repository.CategoryDAO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ConverterCategoryImplTest {
    @Autowired
    private CategoryDAO categoryDAO;
    @Autowired
    private TestEntityManager entityManager;
    private Category categoryParent, subCategory;
    private CategoryDto categoryDto;
    private ConverterCategoryImpl converterCategory;

    @Before
    public void setUp() throws Exception {
        converterCategory = new ConverterCategoryImpl(categoryDAO);
        categoryParent = new Category("category0",null);
        entityManager.persistAndFlush(categoryParent);
        subCategory = new Category("category1", categoryParent);
        entityManager.persistAndFlush(subCategory);
    }

    @After
    public void tearDown() throws Exception {
        this.entityManager.clear();
    }

    @Test
    public void convertModelToDtoParentCategorySuccessfully() {
        categoryDto = converterCategory.convertModelToDto(categoryParent);
        assertEquals(categoryParent.getId(), categoryDto.getId());
        assertEquals(categoryParent.getName(), categoryDto.getName());
        assertEquals(null, categoryDto.getIdParentCategory());
    }

    @Test
    public void convertModelToDtoSubCategorySuccessfully() {
        categoryDto = converterCategory.convertModelToDto(subCategory);
        assertEquals(subCategory.getId(), subCategory.getId());
        assertEquals(subCategory.getName(), subCategory.getName());
        assertEquals(categoryParent.getId(), categoryDto.getIdParentCategory());
    }

    @Test
    public void convertDtoToModelSuccessfully() {
        categoryDto = new CategoryDto();
        categoryDto.setId(subCategory.getId());
        categoryDto.setName("category1");
        categoryDto.setIdParentCategory(categoryParent.getId());
        Category category = converterCategory.convertDtoToModel(categoryDto);
        assertEquals(categoryDto.getId(),category.getId());
        assertEquals(categoryDto.getName(), category.getName());
        assertEquals(categoryDto.getIdParentCategory(),categoryParent.getId());
    }
}