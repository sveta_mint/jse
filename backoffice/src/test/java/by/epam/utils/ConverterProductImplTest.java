package by.epam.utils;

import by.epam.dto.ProductDto;
import by.epam.model.Category;
import by.epam.model.Currency;
import by.epam.model.Price;
import by.epam.model.Product;
import by.epam.repository.CategoryDAO;
import by.epam.repository.PriceDAO;
import by.epam.repository.ProductDAO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ConverterProductImplTest {
    private ConverterProductImpl converterProduct;
    private Product product;
    private ProductDto productDto;
    private Price price;
    private Category category;
    @Autowired
    private PriceDAO priceDAO;
    @Autowired
    private CategoryDAO categoryDAO;
    @Autowired
    private TestEntityManager entityManager;

    @Before
    public void setUp() throws Exception {
        converterProduct = new ConverterProductImpl(priceDAO, categoryDAO);
        price = new Price(20, Currency.RUB);
        category = new Category("category1", null);
        entityManager.persistAndFlush(price);
        entityManager.persistAndFlush(category);
        product = new Product("product1", Arrays.asList(price),Arrays.asList(category));
        entityManager.persistAndFlush(product);
    }
    @After
    public void tearDown() throws Exception {
        this.entityManager.clear();
    }

    @Test
    public void convertModelToDtoSuccessfully() {
        productDto = converterProduct.convertModelToDto(product);
        assertEquals(product.getId(), productDto.getId());
        assertEquals("product1", productDto.getName());
        assertEquals(Arrays.asList(price.getId()), productDto.getPricesId());
        assertEquals(Arrays.asList(category.getId()), productDto.getCategoriesId());
    }

    @Test
    public void convertDtoToModelSuccessfully() {
        productDto = new ProductDto();
        productDto.setId(product.getId());
        productDto.setName("product1");
        productDto.setPricesId(Arrays.asList(price.getId()));
        productDto.setCategoriesId(Arrays.asList(category.getId()));
        Product pr = converterProduct.convertDtoToModel(productDto);
        assertEquals(productDto.getId(), pr.getId());
        assertEquals(productDto.getName(),pr.getName());
        assertArrayEquals(productDto.getPricesId().toArray(), pr.getPrices()
                .stream()
                .map(p -> p.getId())
                .toArray());
        assertArrayEquals(productDto.getCategoriesId().toArray(), pr.getCategories()
                .stream()
                .map(c -> c.getId())
                .toArray());
    }
}