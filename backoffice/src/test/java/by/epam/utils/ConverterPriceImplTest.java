package by.epam.utils;

import by.epam.dto.PriceDto;
import by.epam.model.Currency;
import by.epam.model.Price;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ConverterPriceImplTest {
    private ConverterPriceImpl converterPrice;
    private Price price;
    private PriceDto priceDto;

    @Before
    public void setUp() throws Exception {
        price = new Price(100, Currency.USD);
        converterPrice = new ConverterPriceImpl();
    }

    @Test
    public void convertModelToDtoSuccessfully() {
        priceDto = converterPrice.convertModelToDto(price);
        assertEquals(new Integer(100), priceDto.getValue());
        assertEquals(Currency.USD, priceDto.getCurrency());
    }

    @Test
    public void convertModelToDtoByWrongValue() {
        priceDto = converterPrice.convertModelToDto(price);
        assertNotEquals(new Integer(50), priceDto.getValue());
        assertEquals(Currency.USD, priceDto.getCurrency());
    }

    @Test
    public void convertModelToDtoByWrongCurrency() {
        priceDto = converterPrice.convertModelToDto(price);
        assertEquals(new Integer(100), priceDto.getValue());
        assertNotEquals(Currency.BYN, priceDto.getCurrency());
    }

    @Test
    public void convertDtoToModelSuccessfully() {
        priceDto = new PriceDto();
        priceDto.setId(1L);
        priceDto.setValue(100);
        priceDto.setCurrency(Currency.RUB);
        price = converterPrice.convertDtoToModel(priceDto);
        assertEquals(new Integer(100), price.getValue());
        assertEquals(Currency.RUB, price.getCurrency());
    }

    @Test
    public void convertDtoToModelByWrongValue() {
        priceDto = new PriceDto();
        priceDto.setId(1L);
        priceDto.setValue(100);
        priceDto.setCurrency(Currency.RUB);
        price = converterPrice.convertDtoToModel(priceDto);
        assertNotEquals(new Integer(50), price.getValue());
        assertEquals(Currency.RUB, price.getCurrency());
    }

    @Test
    public void convertDtoToModelByWrongCurrency() {
        priceDto = new PriceDto();
        priceDto.setId(1L);
        priceDto.setValue(100);
        priceDto.setCurrency(Currency.RUB);
        price = converterPrice.convertDtoToModel(priceDto);
        assertEquals(new Integer(100), price.getValue());
        assertNotEquals(Currency.BYN, price.getCurrency());
    }
}