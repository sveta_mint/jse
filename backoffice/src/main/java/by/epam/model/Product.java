package by.epam.model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@Entity(name = "product")
public class Product implements IModel{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name="name", nullable = false)
    private String name;

    @ManyToMany
    @JoinTable(name = "product_price", joinColumns=@JoinColumn (name="product_id"),
            inverseJoinColumns = @JoinColumn (name="price_id"))
    private List<Price> prices = new ArrayList();

    @ManyToMany
    @JoinTable(name = "product_category", joinColumns=@JoinColumn (name="product_id"),
            inverseJoinColumns = @JoinColumn (name="category_id"))
    private List<Category> categories = new ArrayList();

    public Product (String name, List<Price> prices, List<Category> categories) {
        this.name = name;
        this.prices = prices;
        this.categories = categories;
    }

}
