package by.epam.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@Entity(name = "Price")
public class Price implements IModel{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Integer value;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Currency currency;

    public Price (int value, Currency currency) {
        this.value = value;
        this.currency = currency;
    }

    public Price (Long id,int value, Currency currency) {
        this.id = id;
        this.value = value;
        this.currency = currency;
    }
}