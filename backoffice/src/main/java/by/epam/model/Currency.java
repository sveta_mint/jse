package by.epam.model;

import javax.persistence.Embeddable;

public enum Currency {
   USD, BYN, RUB;
}
