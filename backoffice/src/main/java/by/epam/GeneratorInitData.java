package by.epam;

import by.epam.model.Currency;

public class GeneratorInitData {
    public String generatorName(int count, String name) {
        StringBuilder stringBuilder = new StringBuilder(name);
        return  stringBuilder.append((int) (Math.random() * count)).toString();
    }

    public int generatorPrice(int count) {
        return (int) (Math.random() * count);
    }

    public Currency generatorCurrency() {
        Currency[] currencies = Currency.values();
        return currencies[(int) (Math.random() * currencies.length)];
    }

}
