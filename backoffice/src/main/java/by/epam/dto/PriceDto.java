package by.epam.dto;

import by.epam.model.Currency;
import lombok.Data;


@Data
public class PriceDto implements IDto {
    private Long id;
    private Integer value;
    private Currency currency;
}
