package by.epam.dto;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Data
public class ProductDto implements IDto {
    private Long id;
    private String name;
    private List<Long> pricesId = new ArrayList<>();
    private List<Long> categoriesId = new ArrayList<>();
}
