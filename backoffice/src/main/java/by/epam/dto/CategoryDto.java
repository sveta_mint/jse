package by.epam.dto;
import lombok.Data;


@Data
public class CategoryDto implements IDto {
    private Long id;
    private String name;
    private Long idParentCategory;
}
