package by.epam.facade;

import by.epam.dto.IDto;
import by.epam.model.IModel;
import by.epam.repository.ModelRepositoryDAO;
import by.epam.utils.IConverter;
import org.springframework.data.domain.Pageable;
import java.util.List;

public abstract class AbstractModelFacade <M extends IModel, D extends IDto> {
    protected ModelRepositoryDAO<M, Long> repository;
    protected IConverter<M, D> converter;

    public AbstractModelFacade(ModelRepositoryDAO<M, Long> repository, IConverter<M, D> converter) {
        this.converter = converter;
        this.repository = repository;
    }

    public List<D> getAllModels(Pageable pageable) {
        return converter.getListConvertModelDto(repository.findAll(pageable).getContent());
    }

    public D getModelById(Long id) {
        return converter.convertModelToDto(repository.findById(id).orElse(null));
    }

    public void deleteModel(Long id){
        if (repository.existsById(id)) {
            repository.deleteById(id);
        }
    }

    public D createModel(D modelDto) {
        return converter.convertModelToDto(repository.save(converter.convertDtoToModel(modelDto)));
    }

    public D editModel(D modelDto) {
        if (repository.existsById(modelDto.getId())) {
            M m = repository.save(converter.convertDtoToModel(modelDto));
            return converter.convertModelToDto(m);
        }
        return null;
    }
}
