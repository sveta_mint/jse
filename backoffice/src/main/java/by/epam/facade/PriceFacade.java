package by.epam.facade;

import by.epam.dto.PriceDto;
import by.epam.model.Currency;
import by.epam.model.Price;
import by.epam.model.Product;
import by.epam.repository.PriceDAO;
import by.epam.repository.ProductDAO;
import by.epam.utils.IConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public class PriceFacade extends AbstractModelFacade<Price, PriceDto> {
    private ProductDAO productDAO;

    @Autowired
    public PriceFacade(PriceDAO repository, IConverter<Price, PriceDto> converter, ProductDAO productDAO) {
        super(repository, converter);
        this.productDAO = productDAO;
    }

    public List<PriceDto> getPricesByCurrency(Currency currency, Pageable pageable) {
        return converter.getListConvertModelDto(((PriceDAO)repository).findPricesByCurrency(currency, pageable));
    }

    public List<PriceDto> getPriceByProduct(Long id) {
        return converter.getListConvertModelDto(productDAO.findById(id).orElse(new Product()).getPrices());
    }

    public List<PriceDto> getPriceByRange(int minValue, int maxValue, Currency currency, Pageable pageable) {
        return converter.getListConvertModelDto(((PriceDAO)repository).findPricesByValueBetweenAndCurrency(minValue, maxValue, currency, pageable));
    }

}
