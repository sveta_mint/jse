package by.epam.facade;

import by.epam.dto.CategoryDto;
import by.epam.model.Category;
import by.epam.model.Product;
import by.epam.repository.CategoryDAO;
import by.epam.repository.ProductDAO;
import by.epam.utils.IConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CategoryFacade extends AbstractModelFacade<Category, CategoryDto> {
    private ProductDAO productDAO;
    @Autowired
    public CategoryFacade(CategoryDAO repository, IConverter<Category, CategoryDto> converter, ProductDAO productDAO) {
        super(repository, converter);
        this.productDAO = productDAO;
    }

    public List<CategoryDto> getCategoriesByName(String name, Pageable pageable) {
        return converter.getListConvertModelDto(((CategoryDAO)repository).findCategoryByName(name, pageable));
    }

}
