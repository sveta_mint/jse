package by.epam.facade;

import by.epam.dto.ProductDto;
import by.epam.model.Category;
import by.epam.model.Currency;
import by.epam.model.Price;
import by.epam.model.Product;
import by.epam.repository.CategoryDAO;
import by.epam.repository.PriceDAO;
import by.epam.repository.ProductDAO;
import by.epam.utils.IConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductFacade extends AbstractModelFacade<Product, ProductDto> {
    private PriceDAO priceDAO;
    private CategoryDAO categoryDAO;

    @Autowired
    public ProductFacade(ProductDAO repository, IConverter<Product, ProductDto> converter, CategoryDAO categoryDAO, PriceDAO priceDAO) {
        super(repository, converter);
        this.priceDAO = priceDAO;
        this.categoryDAO = categoryDAO;
    }
    public List<ProductDto> getProductsByName(String name, Pageable pageable) {
        return converter.getListConvertModelDto(((ProductDAO)repository).findProductsByName(name, pageable));
    }

    public List<ProductDto> getProductsByPrice (Long id, Pageable pageable) {
        if (priceDAO.existsById(id)) {
            return converter.getListConvertModelDto(((ProductDAO)repository).findProductsByPrices(
                    priceDAO.findById(id).get(), pageable));
        }
        return null;
    }

    public List<ProductDto> getProductsByIdCategory(Long id, Pageable pageable) {
        if(categoryDAO.existsById(id)) {
            return converter.getListConvertModelDto(((ProductDAO)repository).findProductsByCategories(categoryDAO.findById(id).get(),pageable));
        }
        return null;
    }

}
