package by.epam.controller;

import by.epam.dto.PriceDto;
import by.epam.facade.AbstractModelFacade;
import by.epam.facade.PriceFacade;
import by.epam.model.Currency;
import by.epam.model.Price;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/price", method = {RequestMethod.GET, RequestMethod.DELETE,  RequestMethod.POST, RequestMethod.PUT})
public class PriceController extends AbstractCrudController<Price, PriceDto> {
    @Autowired
    public PriceController(PriceFacade priceFacade) {
        super(priceFacade);
    }

    @GetMapping("/search/currency")
    public @ResponseBody ResponseEntity<List<PriceDto>> findPricesByCurrency(@RequestParam (value = "currency") Currency currency, Pageable pageable) {
        List<PriceDto> priceDto = ((PriceFacade)abstractModelFacade).getPricesByCurrency(currency, pageable);
        return new ResponseEntity<>(priceDto, HttpStatus.OK);
    }

    @GetMapping("/search/product/{idProduct}")
    public @ResponseBody ResponseEntity<List<PriceDto>> getPricesByProduct(@PathVariable Long idProduct) {
        List<PriceDto> priceDto = ((PriceFacade)abstractModelFacade).getPriceByProduct(idProduct);
        return new ResponseEntity<>(priceDto, HttpStatus.OK);
    }

    @GetMapping("/search/range")
    public @ResponseBody ResponseEntity<List<PriceDto>> getPricesByRange(@RequestParam (value ="minAmount") int minValue, @RequestParam (value ="maxAmount") int maxValue,
           @RequestParam  (value ="currency") Currency currency, Pageable pageable) {
        List<PriceDto> priceDto = ((PriceFacade)abstractModelFacade).getPriceByRange(minValue, maxValue, currency, pageable);
        return new ResponseEntity<>(priceDto, HttpStatus.OK);
    }


}
