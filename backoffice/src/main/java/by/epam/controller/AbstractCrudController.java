package by.epam.controller;

import by.epam.dto.IDto;
import by.epam.facade.AbstractModelFacade;
import by.epam.model.IModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import org.springframework.data.domain.Pageable;
import java.util.List;

public class AbstractCrudController <M extends IModel, D extends IDto> {
    protected AbstractModelFacade <M, D> abstractModelFacade;

    public AbstractCrudController(AbstractModelFacade<M, D> abstractModelFacade) {
        this.abstractModelFacade = abstractModelFacade;
    }

    @GetMapping("/all")
    public @ResponseBody  ResponseEntity<List<D>> findAllModels(Pageable pageable) {
        List<D> modelDto = abstractModelFacade.getAllModels(pageable);
        return new ResponseEntity<>(modelDto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public @ResponseBody ResponseEntity<Void> deleteProduct(@PathVariable Long id) {
        if (abstractModelFacade.getModelById(id)!= null) {
            abstractModelFacade.deleteModel(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @PostMapping(value = "/add")
    public @ResponseBody ResponseEntity<D> addProduct(@RequestBody D dto) {
        D modelDto = abstractModelFacade.createModel(dto);
        return new ResponseEntity<>(modelDto, HttpStatus.CREATED);
    }

    @PutMapping("/edit")
    public @ResponseBody ResponseEntity<D> editProduct(@RequestBody D dto){
        D modelDto = abstractModelFacade.editModel(dto);
        return new ResponseEntity<>(modelDto, HttpStatus.OK);
    }

    @GetMapping("/id/{id}")
    public @ResponseBody ResponseEntity<D> getProduct(@PathVariable Long id){
        D dto = abstractModelFacade.getModelById(id);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

}
