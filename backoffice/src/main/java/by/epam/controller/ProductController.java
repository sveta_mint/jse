package by.epam.controller;

import by.epam.dto.ProductDto;
import by.epam.facade.AbstractModelFacade;
import by.epam.facade.ProductFacade;
import by.epam.model.Currency;
import by.epam.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping(value = "/product", method = {RequestMethod.GET, RequestMethod.DELETE,  RequestMethod.POST, RequestMethod.PUT})
public class ProductController extends AbstractCrudController<Product, ProductDto>{
    @Autowired
    public ProductController(ProductFacade productFacade) {
        super(productFacade);
    }

    @GetMapping("/search/name/{name}")
    public @ResponseBody ResponseEntity<List<ProductDto>> getProductByName(@PathVariable String name, Pageable pageable) {
        List<ProductDto> productDto = ((ProductFacade)abstractModelFacade).getProductsByName(name, pageable);
        return new ResponseEntity<>(productDto, HttpStatus.OK);
    }

    @GetMapping("/search/price/{idPrice}")
    public @ResponseBody ResponseEntity<List<ProductDto>> getProductByPrice(@PathVariable Long idPrice, Pageable pageable) {
        List<ProductDto> dto = ((ProductFacade)abstractModelFacade).getProductsByPrice(idPrice, pageable);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping("/search/category/{id}")
    public @ResponseBody ResponseEntity<List<ProductDto>> getProductByIdCategory(@PathVariable Long id, Pageable pageable) {
        List<ProductDto> productDto =((ProductFacade)abstractModelFacade).getProductsByIdCategory(id, pageable);
        return new ResponseEntity<>(productDto, HttpStatus.OK);
    }
}
