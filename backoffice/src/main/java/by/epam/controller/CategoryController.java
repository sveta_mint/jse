package by.epam.controller;

import by.epam.dto.CategoryDto;
import by.epam.dto.ProductDto;
import by.epam.facade.AbstractModelFacade;
import by.epam.facade.CategoryFacade;
import by.epam.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/category", method = {RequestMethod.GET, RequestMethod.DELETE,  RequestMethod.POST, RequestMethod.PUT})
public class CategoryController extends AbstractCrudController<Category, CategoryDto>{
    @Autowired
    public CategoryController(CategoryFacade categoryFacade) {
        super(categoryFacade);
    }

    @GetMapping("/search/name/{name}")
    public @ResponseBody ResponseEntity<List<CategoryDto>> getCategoriesByName(@PathVariable String name, Pageable pageable) {
        List<CategoryDto> categoryDto = ((CategoryFacade)abstractModelFacade).getCategoriesByName(name, pageable);
        return new ResponseEntity<>(categoryDto, HttpStatus.OK);
    }
}
