package by.epam.utils;

import by.epam.dto.CategoryDto;
import by.epam.model.Category;
import by.epam.repository.CategoryDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class ConverterCategoryImpl implements IConverter<Category,CategoryDto> {
    private CategoryDAO categoryDAO;

    @Autowired
    public ConverterCategoryImpl(CategoryDAO categoryDAO) {
        this.categoryDAO = categoryDAO;
    }

    @Override
    public CategoryDto convertModelToDto(Category category) {
        if (category == null) {
            return null;
        }
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setId(category.getId());
        categoryDto.setName(category.getName());
        categoryDto.setIdParentCategory(category.getParentCategory()!= null ? category.getParentCategory().getId() : null);
        return categoryDto;
    }

    @Override
    public Category convertDtoToModel(CategoryDto dto) {
        if (dto == null) {
            return null;
        }
        Category category = new Category();
        category.setId(dto.getId());
        category.setName(dto.getName());
        category.setParentCategory(dto.getIdParentCategory()!=null ? categoryDAO.findById(dto.getIdParentCategory()).orElse(null) : null);
        return category;
    }

}
