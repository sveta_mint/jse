package by.epam.utils;

import by.epam.dto.PriceDto;
import by.epam.model.Price;
import org.springframework.stereotype.Component;


@Component
public class ConverterPriceImpl implements IConverter<Price, PriceDto> {
    @Override
    public PriceDto convertModelToDto(Price price) {
        if (price == null) {
            return null;
        }
        PriceDto priceDto = new PriceDto();
        priceDto.setId(price.getId());
        priceDto.setValue(price.getValue());
        priceDto.setCurrency(price.getCurrency());
        return priceDto;
    }

    @Override
    public Price convertDtoToModel(PriceDto dto) {
        if (dto == null) {
            return null;
        }
        Price price = new Price();
        price.setId(dto.getId());
        price.setValue(dto.getValue());
        price.setCurrency(dto.getCurrency());
        return price;
    }

}
