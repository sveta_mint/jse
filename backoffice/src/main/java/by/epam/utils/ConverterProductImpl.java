package by.epam.utils;

import by.epam.dto.ProductDto;
import by.epam.model.Category;
import by.epam.model.Price;
import by.epam.model.Product;
import by.epam.repository.CategoryDAO;
import by.epam.repository.PriceDAO;
import by.epam.repository.ProductDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;


@Component
public class ConverterProductImpl implements IConverter<Product, ProductDto> {
    private PriceDAO priceDAO;
    private CategoryDAO categoryDAO;

    @Autowired
    public ConverterProductImpl(PriceDAO priceDAO, CategoryDAO categoryDAO) {
        this.priceDAO = priceDAO;
        this.categoryDAO = categoryDAO;
    }
    @Override
    public ProductDto convertModelToDto(Product product) {
        if (product == null) {
            return null;
        }
        ProductDto productDto = new ProductDto();
        productDto.setId(product.getId());
        productDto.setName(product.getName());
        productDto.setPricesId(getListModelId(product.getPrices()));
        productDto.setCategoriesId(getListModelId(product.getCategories()));
        return productDto;
    }

    @Override
    public Product convertDtoToModel(ProductDto productDto) {
        if (productDto == null) {
            return null;
        }
        Product product = new Product();
        product.setId(productDto.getId());
        product.setName(productDto.getName());
        product.setPrices(priceDAO.findAllById(productDto.getPricesId()));
        product.setCategories(categoryDAO.findAllById(productDto.getCategoriesId()));
        return product;
    }

}
