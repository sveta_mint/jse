package by.epam.utils;

import by.epam.dto.IDto;
import by.epam.model.IModel;
import java.util.List;
import java.util.stream.Collectors;

public interface IConverter<M extends IModel, D extends IDto> {
    D convertModelToDto(M model);
    M convertDtoToModel (D dto);
    default List<D> getListConvertModelDto (List<M> models) {
        return models
                .stream()
                .map(model -> convertModelToDto(model))
                .collect(Collectors.toList());
    }

    default List<Long> getListModelId(List<? extends IModel> models) {
        return models
                .stream()
                .map(IModel :: getId)
                .collect(Collectors.toList());
    }
}
