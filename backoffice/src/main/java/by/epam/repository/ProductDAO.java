package by.epam.repository;

import by.epam.model.Category;
import by.epam.model.Price;
import by.epam.model.Product;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductDAO extends ModelRepositoryDAO<Product,Long> {
    List<Product> findProductsByName(String name, Pageable pageable);
    List<Product> findProductsByPrices(Price price, Pageable pageable);
    List<Product> findProductsByCategories(Category category, Pageable pageable);
}
