package by.epam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;


@NoRepositoryBean
public interface ModelRepositoryDAO<IModel, Long> extends JpaRepository <IModel,Long> {
}
