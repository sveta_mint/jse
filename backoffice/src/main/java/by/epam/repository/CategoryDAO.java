package by.epam.repository;

import by.epam.model.Category;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryDAO extends ModelRepositoryDAO <Category,Long> {
    List<Category> findCategoryByName(String name, Pageable pageable);
}