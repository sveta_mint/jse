package by.epam.repository;

import by.epam.model.Currency;
import by.epam.model.Price;
import org.springframework.data.domain.Pageable;

import java.util.List;


public interface PriceDAO extends ModelRepositoryDAO<Price,Long> {
    List<Price> findPricesByCurrency(Currency currency, Pageable pageable);
    List<Price> findPricesByValueBetweenAndCurrency(int minValue, int maxValue,Currency currency, Pageable pageable);
}
