package by.epam;

/**
 * contains methods for working with cache
 * @param <K> key
 * @param <V> value
 */
public interface Cache<K, V> {
    /**
     * adding an item to the cache
     * @param key
     * @param value
     */
    void put(K key, V value);

    /**
     * retrieving item value by key
     * @param key
     * @return value
     */
    V get(K key);
}
