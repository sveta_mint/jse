package by.epam;

import java.util.*;


/**
 * LFU cache algorithm
 * @param <K> key
 * @param <V> value
 */
public class LfuCache<K, V> implements Cache<K, V> {
    private final int size;
    private final Map<K, EntryCache> cacheMap;

    /**
     * Constructor of lfu class
     * @param size cache size
     */
    public LfuCache(final int size) {
        this.size = size;
        this.cacheMap = new LinkedHashMap<>(size);
    }

    /**
     * Verification of full of the cache
     * @return true if the cache is full
     */
    private boolean isFullSize() {
        if (cacheMap.size() == size) {
            return true;
        }
        return false;
    }

    /**
     * eviction from cache if this need
     */
    private void evictionCache() {
        K key = cacheMap.entrySet().stream()
                .min((entry1, entry2) -> entry1.getValue().getFrequency() - entry2.getValue().getFrequency())
                .get()
                .getKey();

        cacheMap.remove(key);
    }

    /**
     * adding an item to the cache
     * @param key item key
     * @param value value of element by key
     */
    public synchronized void put(final K key, final V value) {
        if (isFullSize()) {
            evictionCache();
        }
        cacheMap.put(key, new EntryCache(value));
    }

    /**
     * retrieving item value by key
     * @param key item key
     * @return value
     */
    public synchronized V get(final K key) {
        EntryCache entryCache = cacheMap.get(key);
        return entryCache == null ? null : entryCache.incrementFrequency().getData();
    }

    private final class EntryCache {
        private V data;
        private int frequency;

        private EntryCache(final V value) {
            this.data = value;
            this.frequency = 1;
        }

        public EntryCache incrementFrequency() {
            frequency++;
            return this;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            EntryCache that = (EntryCache) o;
            return getFrequency() == that.getFrequency() &&
                    Objects.equals(getData(), that.getData());
        }

        @Override
        public int hashCode() {
            return Objects.hash(getData(), getFrequency());
        }

        @Override
        public String toString() {
            return data + " frequency: " + frequency;
        }

        public V getData() {
            return data;
        }

        public void setData(final V data) {
            this.data = data;
        }

        public int getFrequency() {
            return frequency;
        }

        public void setFrequency(final int frequency) {
            this.frequency = frequency;
        }
    }

    @Override
    public String toString() {
        return "Lfucache: " + cacheMap + " size cache: " + size;
    }
}
