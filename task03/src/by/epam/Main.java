package by.epam;

/**
 * Class for testing algorithms lru and lfu
 */
public class Main {
    /**
     * entry method program
     * @param args
     */
    public static void main(final String[] args) {
        Cache<Integer, Integer> lruCache = new LruCache<Integer, Integer>(6, 0.75f) {
            { put(1, 1); put(2, 2); put(3, 3); put(4, 5); }
        };
        Cache<Integer, Integer> lfuCache = new LfuCache<Integer, Integer>(4) {
            { put(1, 1); put(2, 2); put(3, 3); put(4, 4); }
        };
        Thread threadFirst = new Thread(()-> {
            lruCache.put(1,10);
            lruCache.put(5,6);
            lruCache.get(1);
            lruCache.get(2);
        });
        Thread threadSecond = new Thread(() -> {
            lruCache.put(7,7);
            lruCache.get(1);
        });

        Thread threadThird = new Thread(() -> {
            lfuCache.get(1);
            lfuCache.get(2);
            lfuCache.get(1);
            lfuCache.get(4);
        });
        Thread threadFourth = new Thread (() -> {
            lfuCache.get(4);
            lfuCache.get(3);
            lfuCache.get(2);
            lfuCache.put(6, 6);
        });
        threadFirst.start();
        threadSecond.start();
        threadThird.start();
        threadFourth.start();
        try {
            threadFirst.join();
            threadSecond.join();
            threadThird.join();
            threadFourth.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(lruCache);
        System.out.println(lfuCache);
    }
    /**
     * Default constructor of testing class
     */
    public Main() {
    }
}
