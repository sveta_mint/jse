package by.epam;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * LRU cache algorithm
 * @param <K> key
 * @param <V> value
 */
public class LruCache<K, V>  implements Cache<K, V> {
    private static final boolean ACCESS_ORDER = true;
    private int size;
    private float loadFactor;
    private Map<K, V> cacheMap;

    /**
     * Constructor of lru class
     * @param size cache size
     * @param loadFactor of cache
     */
    public LruCache(final int size, final float loadFactor) {
        this.cacheMap  = new LinkedHashMap<K, V>(size, loadFactor, ACCESS_ORDER) {
            @Override
            protected boolean removeEldestEntry(final Map.Entry<K, V> eldest) {
                return size() >= size;
            }
        };
    }

    @Override
    public synchronized void put(final K key, final V value) {
        cacheMap.put(key, value);
    }

    @Override
    public synchronized V get(final K key) {
        return cacheMap.get(key);
    }

    @Override
    public String toString() {
        return "Lrucache: " + cacheMap + " size cache: " + size;
    }
}
