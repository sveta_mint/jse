Source data:
              - number of floors of the building (floorsNumber)       
              - Elevator cabin capacity (person) (elevatorCapacity)
              - number of people to transport (passengersNumber)
              
Initializing after you start the application:
              - first, the parameters from the config file are read
              - create the required number (passengersNumber) copies of passengers (Passenger), which are randomly placed on the floors. Each passenger has a property" destination floor " (destinationStory), which is also initialized randomly, of course excluding the original floor on which the passenger is waiting for their transportation. Each Passenger instance is given a unique passengerID
              
The start of the transportation process:
           - for each passenger gets a TranportationTask that implantiruete means Java as a separate Thread              
           - after that, the main Thread of the program begins to work on the cyclic movement of the Elevator between the floors (from the 1st floor to the last) up-down-up-down.
           - all the work on the Elevator control takes on some operator (Controller). For example,as it was in the old elevators, when in the Elevator cabin there was a special person who started/stopped the Elevator, let in and let out people. Thus, the Elevator cabin is a container (evelvatorContainer) of limited size, in which the transported people are placed.
The end of the process of transportation:
            - when will not remain nor one of the containers of the departure of any one person, the transportation process to come to an end and the operator stops the lift. After that, all TransportationTask'and considered complete and all the appropriate Threads, they must be completed.