package by.epam.reader;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class ConfigReaderTest {
    private ConfigReader configReader;
    private ResourceBundle resourceBundle = ResourceBundle.getBundle("config");

    @Before
    public void initConfigReader() {
        configReader = ConfigReader.getConfigReader();
    }

    @After
    public void afterTestConfigReader() {
        configReader = null;
    }

    @Test(expected = MissingResourceException.class)
    public void checkForMissingConfigFile(){
        ResourceBundle.getBundle("");
    }

    @Test
    public void checkConfigFileName() {
        boolean checkResult = resourceBundle.getBaseBundleName().equals("config");
        assertTrue(checkResult);
    }

    @Test
    public void checkKeysInConfigFile() {
        List<String> nameKeys = Arrays.asList("passengersNumber","elevatorCapacity","floorsNumber");
        List<String> keys = Collections.list(resourceBundle.getKeys());
        boolean result = keys.equals(nameKeys);
        assertTrue(result);
    }

    @Test
    public void checkWrongFloorsNumber() {
        assertNotEquals(0, configReader.getFloorsNumber());
    }

    @Test
    public void checkRightFloorsNumber() {
        assertEquals(9,configReader.getFloorsNumber());
    }

    @Test
    public void checkRightPassengersNumber() {
        assertEquals(5,configReader.getPassengersNumber());
    }

    @Test
    public void checkWrongPassengersNumber() {
        assertNotEquals(0, configReader.getPassengersNumber());
    }


    @Test
    public void checkRightElevatorCapacity() {
        assertEquals(3,configReader.getElevatorCapacity());
    }

    @Test
    public void checkWrongElevatorCapacity() {
        assertNotEquals(0, configReader.getElevatorCapacity());
    }


}