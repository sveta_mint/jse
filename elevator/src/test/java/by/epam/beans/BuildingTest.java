package by.epam.beans;

import by.epam.reader.ConfigReader;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BuildingTest {
    private Building building;

    @Before
    public void initBuilding() {
        building = new Building();
    }

    @After
    public void afterTestBuilding() {
        building = null;
    }

    @Test
    public void checkAllArrivedPassenger() {
        building.setPassengersNumber(0);
        assertEquals(0, building.arrived());
    }

    @Test
    public void checkPassengersNumber() {
        ConfigReader configReader = ConfigReader.getConfigReader();
        assertEquals(configReader.getPassengersNumber(), building.getPassengersNumber());
    }

    @Test
    public void checkElevatorCapacity() {
        ConfigReader configReader = ConfigReader.getConfigReader();
        assertEquals(configReader.getElevatorCapacity(), building.getElevator().getElevatorCapacity());
    }

    @Test
    public void checkFloorsNumber() {
        ConfigReader configReader = ConfigReader.getConfigReader();
        assertEquals(configReader.getFloorsNumber(), building.getFloors().size());
    }


}