package by.epam.beans;

import by.epam.enums.Direction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.*;

public class ElevatorTest {

    private Elevator elevator;

    @Before
    public void initElevatorTest() {
        elevator = new Elevator(3, 9);
    }

    @After
    public void afterTest(){
        elevator = null;
    }

    @Test
    public void testIsFullCapacity() {
        elevator.getElevatorContainer().add(new Passenger(1, 2,5));
        elevator.getElevatorContainer().add(new Passenger(2, 3,5));
        elevator.getElevatorContainer().add(new Passenger(3, 4,5));
        assertEquals(elevator.getElevatorCapacity(), elevator.getElevatorContainer().size());
    }

    @Test
    public void testIsUpDirectionPassengerAndElevator() {
        Passenger passenger = new Passenger(1, 6, 9);
        assertTrue(elevator.isSameDirection(passenger));
    }

    @Test
    public void testIsDownDirectionPassengerAndElevator() {
        Passenger passenger = new Passenger(2, 5, 3);
        assertFalse(elevator.isSameDirection(passenger));
    }

    @Test
    public void testMoveNextUpFloorElevator() {
        assertEquals(2, elevator.nextFloor());
    }

    @Test
    public void testMoveNextDownFloorElevator() {
        elevator.setDirection(Direction.DOWN);
        assertEquals(0, elevator.nextFloor());
    }

    @Test
    public void testChooseUpDirectionElevator() {
        Method chooseDirection = null;
        try {
            chooseDirection = Elevator.class.getDeclaredMethod("chooseDirection");
            chooseDirection.setAccessible(true);
            assertEquals(Direction.UP, chooseDirection.invoke(elevator));
        } catch (NoSuchMethodException |IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        } finally {
            chooseDirection.setAccessible(false);
        }
    }

    @Test
    public void testChooseDownDirectionElevator() {
        elevator.setCurrentFloorNumber(elevator.getMaxFloorNumber());
        Method chooseDirection = null;
        try {
            chooseDirection = Elevator.class.getDeclaredMethod("chooseDirection");
            chooseDirection.setAccessible(true);
            assertEquals(Direction.DOWN, chooseDirection.invoke(elevator));
        } catch (NoSuchMethodException |IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        } finally {
            chooseDirection.setAccessible(false);
        }
    }

}