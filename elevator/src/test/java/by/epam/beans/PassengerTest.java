package by.epam.beans;

import by.epam.enums.Direction;
import by.epam.enums.TransportationState;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.*;

public class PassengerTest {
    private Passenger passenger;

    @Before
    public void initPassenger() {
        passenger = new Passenger(1, 2, 5);
    }

    @After
    public void afterTestPassenger() {
        passenger = null;
    }

    @Test
    public void checkUpDirectionPassenger(){
        assertEquals(Direction.UP, passenger.getDirection());
    }

    @Test
    public void checkDownDirectionPassenger() {
        assertNotEquals(Direction.DOWN, passenger.getDirection());
    }

    @Test
    public void checkCurrentAndDestinationFloorPassenger() {
        assertNotEquals(passenger.getCurrentFloor(), passenger.getDestinationFloor());
    }

    @Test
    public void checkInitTransportationStatePassenger() {
        assertEquals(TransportationState.NOT_STARTED, passenger.getTransportationState());
    }
}