package by.epam.action;

import by.epam.beans.Building;
import by.epam.beans.Floor;
import by.epam.beans.Passenger;
import by.epam.enums.TransportationState;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import static org.junit.Assert.*;

public class ControllerTest {
    private Controller controller;
    private Building building;

    @Before
    public void initController() {
        building = new Building();
        controller = new Controller(building);
    }
    @After
    public void afterControllerTest() {
        building = null;
        controller = null;
    }

    @Test
    public void checkCountDispatchPassenger() {
        Method checkCountDispatchPassenger = null;
        try {
            checkCountDispatchPassenger = Controller.class.getDeclaredMethod("checkCountDispatchPassenger");
            checkCountDispatchPassenger.setAccessible(true);
            assertNotEquals(0, checkCountDispatchPassenger.invoke(controller));
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        } finally {
            checkCountDispatchPassenger.setAccessible(false);
        }
    }

    @Test
    public void checkCountArrivalPassenger() {
        Method checkCountArrivalPassenger = null;
        try {
            checkCountArrivalPassenger = Controller.class.getDeclaredMethod("checkCountArrivalPassenger");
            checkCountArrivalPassenger.setAccessible(true);
            assertEquals(0, checkCountArrivalPassenger.invoke(controller));
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        } finally {
            checkCountArrivalPassenger.setAccessible(false);
        }
    }

    @Test
    public void checkStateArrivalPassenger() {
        Method checkStateArrivalPassenger = null;
        try {
            checkStateArrivalPassenger = Controller.class.getDeclaredMethod("checkStateArrivalPassenger");
            checkStateArrivalPassenger.setAccessible(true);
            assertEquals(true, checkStateArrivalPassenger.invoke(controller));
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        } finally {
            checkStateArrivalPassenger.setAccessible(false);
        }
    }

    @Test
    public void checkFloorsArrivalPassenger() {
        Method checkFloorsArrivalPassenger = null;
        try {
            checkFloorsArrivalPassenger = Controller.class.getDeclaredMethod("checkFloorsArrivalPassenger");
            checkFloorsArrivalPassenger.setAccessible(true);
            assertEquals(true, checkFloorsArrivalPassenger.invoke(controller));
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        } finally {
            checkFloorsArrivalPassenger.setAccessible(false);
        }
    }

    @Test
    public void enterToElevator() {
        Passenger passenger = new Passenger(1,3,1);
        Floor currentFloor = building.getFloors().get(0);
        currentFloor.getDispatchFloorContainer().add(passenger);
        controller.enterToElevator(passenger);
        assertTrue(building.getElevator().getElevatorContainer().contains(passenger));
        assertFalse(currentFloor.getDispatchFloorContainer().contains(passenger));
    }

    @Test
    public void exitElevator() {
        Passenger passenger = new Passenger(1,3,1);
        Floor currentFloor = building.getFloors().get(0);
        building.getElevator().getElevatorContainer().add(passenger);
        controller.exitElevator(passenger);
        assertFalse(building.getElevator().getElevatorContainer().contains(passenger));
        assertTrue(currentFloor.getArrivalFloorContainer().contains(passenger));
    }

    @Test
    public void hasAnyPassengerSameDirection() {
        Passenger passenger = new Passenger(6,1,3);
        List<Passenger> passengers = building.getFloors().get(0).getDispatchFloorContainer();
        passengers.add(passenger);
        Method hasAnyPassengerSameDirection = null;
        try {
            hasAnyPassengerSameDirection = Controller.class.getDeclaredMethod("hasAnyPassengerSameDirection",List.class);
            hasAnyPassengerSameDirection.setAccessible(true);
            assertEquals(true, hasAnyPassengerSameDirection.invoke(controller, passengers));
        } catch (NoSuchMethodException  | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        } finally {
            hasAnyPassengerSameDirection.setAccessible(false);
        }
    }

    @Test
    public void checkHasArrived() {
        Passenger passenger = new Passenger(6,1,3);
        List<Passenger> passengers = building.getFloors().get(0).getArrivalFloorContainer();
        passengers.add(passenger);
        building.getElevator().setCurrentFloorNumber(3);
        Method hasArrived = null;
        try {
            hasArrived = Controller.class.getDeclaredMethod("hasArrived",List.class);
            hasArrived.setAccessible(true);
            assertEquals(true, hasArrived.invoke(controller, passengers));
        } catch (NoSuchMethodException  | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        } finally {
            hasArrived.setAccessible(false);
        }
    }
}