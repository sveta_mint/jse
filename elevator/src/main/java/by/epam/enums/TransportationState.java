package by.epam.enums;

/**
 * enumeration for transportation state passenger
 */
public enum TransportationState {
    /**
     * transportation state of the passenger at initialization
     */
    NOT_STARTED,
    /**
     * transportation state of the passenger at elevator
     */
    IN_PROGRESS,
    /**
     * transportation state of the passenger at finish
     */
    COMPLETED;
}
