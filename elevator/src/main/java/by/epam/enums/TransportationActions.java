package by.epam.enums;

/**
 * enumeration for transportation actions elevator and passengers
 */
public enum TransportationActions {
    /**
     * description staring transportation
     */
    STARTING_TRANSPORTATION,
    /**
     * description finish transportation
     */
    COMPLETION_TRANSPORTATION,
    /**
     * description moving elevator
     */
    MOVING_ELEVATOR,
    /**
     * description entering passenger in elevator
     */
    BOARDING_OF_PASSENGER,
    /**
     * description exit passenger from elevator
     */
    DEBOARDING_OF_PASSENGER;
}
