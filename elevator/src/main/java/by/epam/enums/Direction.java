package by.epam.enums;

/**
 * enumeration for direction passenger
 */
public enum Direction {
    /**
     *  direction passenger is Up
     */
    UP,
    /**
     *  direction passenger is Down
     */
    DOWN;
}
