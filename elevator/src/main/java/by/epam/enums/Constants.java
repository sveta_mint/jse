package by.epam.enums;

/**
 * Class for constants application
 */
public final class Constants {
    /**
     * constants for floor number
     */
    public static final String FLOORS_NUMBERS = "floorsNumber";
    /**
     * constants for elevator capacity
     */
    public static final String ELEVATOR_CAPACITY = "elevatorCapacity";
    /**
     * constants for passengers number
     */
    public static final String PASSENGER_NUMBER = "passengersNumber";
    /**
     * constants for config file name
     */
    public static final String NAME_FILE = "config";
    /**
     * constants for not found config file
     */
    public static final String CONFIG_FILE_NOT_FOUND = "Config file not found!";
    /**
     * constants for message Wrong init data
     */
    public static final String WRONG_INIT_DATA = "Wrong init data!";
    /**
     * constants for floor elevator
     */
    public static final String FROM_FLOOR = " from floor ";
    /**
     * constants for floor elevator
     */
    public static final String TO_FLOOR = " to floor ";
    /**
     * constants for current floor passenger
     */
    public static final String CURRENT_FLOOR = "CURRENT FLOOR: ";
    /**
     * constants for validation transportation
     */
    public static final String SUM_SIZE_DISPATCH_CONTAINER = "SUM_SIZE_DISPATCH_CONTAINER ";
    /**
     * constants for validation transportation
     */
    public static final String SUM_SIZE_ARRIVAL_CONTAINER = "SUM_SIZE_ARRIVAL_CONTAINER ";
    /**
     * constants for elevator
     */
    public static final String ELEVATOR = "ELEVATOR ";
    /**
     * constants for passengers number
     */
    public static final String INITAL_COUNT_PASSENGER = "INITAL_COUNT_PASSENGER ";
    /**
     * constants for arrival passengers number
     */
    public static final String FINAL_COUNT_PASSENGER = " FINAL_COUNT_PASSENGER ";
    /**
     * constants for validation transportation
     */
    public static final String VALIDATE_RESULT = "VALIDATE_RESULT: ";
    /**
     * constants for index
     */
    public static final int ONE = 1;

    private Constants() {

    }
}
