package by.epam.action;

import by.epam.beans.*;
import by.epam.enums.TransportationActions;
import by.epam.enums.TransportationState;
import org.apache.log4j.Logger;

import java.util.List;
import static by.epam.enums.Constants.*;

/**
 *  Class describing the controller in the building
 */
public class Controller {
    private Building building;
    private Elevator elevator;
    private int fromFloor, toFloor;
    private static final Logger LOGGER = Logger.getLogger(Controller.class);

    /**
     * Constructor for creating controller
     * @param building building
     */
    public Controller (final Building building) {
        this.building = building;
        elevator = building.getElevator();
    }

    /**
     * method describing how the controller moves the elevator
     * @param building
     */
    public void moveElevator(final Building building) {
        LOGGER.info(TransportationActions.STARTING_TRANSPORTATION);
        System.out.println(TransportationActions.STARTING_TRANSPORTATION);
        while (!building.allArrived()) {
            movePassengers(building);
            fromFloor = elevator.getCurrentFloorNumber();
            elevator.nextFloor();
            toFloor = elevator.getCurrentFloorNumber();
            LOGGER.info(TransportationActions.MOVING_ELEVATOR + FROM_FLOOR + fromFloor + TO_FLOOR + toFloor);
            System.out.println(TransportationActions.MOVING_ELEVATOR + FROM_FLOOR + fromFloor + TO_FLOOR + toFloor);
        }
        validateTransportation();
    }

    private void movePassengers(final Building building) {
        int currentFloor = elevator.getCurrentFloorNumber() - ONE;
        Floor curFloor = building.getFloors().get(currentFloor);
        System.out.println(CURRENT_FLOOR + currentFloor);
        List<Passenger> dispatchContainer = building.getFloors().get(currentFloor).getDispatchFloorContainer();
        List<Passenger> elevatorContainer = elevator.getElevatorContainer();
        try {
            synchronized (elevator) {
                elevator.notifyAll();
                while (hasArrived(elevatorContainer)) {
                    elevator.wait();
                }
            }
            synchronized (curFloor) {
                curFloor.notifyAll();
                while (hasAnyPassengerSameDirection(dispatchContainer) && !building.getElevator().isFullCapacity()) {
                    curFloor.wait();
                }
            }
        } catch (InterruptedException e) {
            LOGGER.error(e);
        }
    }

    private boolean hasArrived(final List<Passenger> arrivedPassengers) {
        return arrivedPassengers.stream()
                .anyMatch(passenger -> passenger.getDestinationFloor() == elevator.getCurrentFloorNumber());
    }

    private boolean hasAnyPassengerSameDirection(final List<Passenger> dispatchPassengers) {
        return dispatchPassengers.stream()
                .anyMatch(passenger -> elevator.isSameDirection(passenger));
    }

    /**
     * method describing how the controller lets passengers into the elevator
     * @param passenger passenger
     */
    public void enterToElevator(final Passenger passenger) {
        int currentFloor = elevator.getCurrentFloorNumber() - ONE;
        Floor floor = building.getFloors().get(currentFloor);
        synchronized (floor) {
            elevator.getElevatorContainer().add(passenger);
            building.getFloors().get(currentFloor).getDispatchFloorContainer().remove(passenger);
            floor.notifyAll();
        }
    }

    /**
     * method describing how the controller lets passengers out of the elevator
     * @param passenger passenger
     */
    public void exitElevator(final Passenger passenger) {
        int currentFloor = building.getElevator().getCurrentFloorNumber() - ONE;
        synchronized (elevator) {
            building.getFloors().get(currentFloor).getArrivalFloorContainer().add(passenger);
            elevator.getElevatorContainer().remove(passenger);
            elevator.notifyAll();
            building.arrived();
        }
    }
    private void validateTransportation() {
        boolean result = true;
        int initialPassengersNumber = building.getPassengersNumber();
        int countDispatchPassengers = checkCountDispatchPassenger();
        if (countDispatchPassengers != 0) {
            result = false;
        }
        LOGGER.info(SUM_SIZE_DISPATCH_CONTAINER + countDispatchPassengers);
        System.out.println(SUM_SIZE_DISPATCH_CONTAINER + countDispatchPassengers);

        int countElevatorPassengers = checkCountPassengerInElevator();
        if (countElevatorPassengers != 0) {
            result = false;
        }
        LOGGER.info(ELEVATOR + countElevatorPassengers);
        System.out.println(ELEVATOR + countElevatorPassengers);
        if (!checkStateArrivalPassenger() && !checkFloorsArrivalPassenger()) {
            result = false;
        }
        int countArrivalPassenger = checkCountArrivalPassenger();
        if (countArrivalPassenger != initialPassengersNumber ) {
            result = false;
        }
        LOGGER.info(SUM_SIZE_ARRIVAL_CONTAINER + countArrivalPassenger);
        System.out.println(SUM_SIZE_ARRIVAL_CONTAINER + countArrivalPassenger);
        LOGGER.info(INITAL_COUNT_PASSENGER + initialPassengersNumber + FINAL_COUNT_PASSENGER + building.getPassengersNumber());
        System.out.println(INITAL_COUNT_PASSENGER + initialPassengersNumber + FINAL_COUNT_PASSENGER + building.getPassengersNumber());
        LOGGER.info(VALIDATE_RESULT + result);
        System.out.println(VALIDATE_RESULT + result);
    }

    private int checkCountDispatchPassenger() {
        return building.getFloors().stream()
                .mapToInt(floor -> floor.getDispatchFloorContainer().size())
                .sum();
    }

    private int checkCountPassengerInElevator() {
        return elevator.getElevatorContainer().size();
    }

    private int checkCountArrivalPassenger() {
        return building.getFloors().stream()
                .mapToInt(floor -> floor.getArrivalFloorContainer().size())
                .sum();
    }

    private boolean checkStateArrivalPassenger() {
        return building.getFloors().stream().allMatch(floor->floor.getArrivalFloorContainer()
                .stream()
                .allMatch(passenger ->
                passenger.getTransportationState()==TransportationState.COMPLETED));
    }

    private boolean checkFloorsArrivalPassenger() {
        return building.getFloors().stream()
                .allMatch(floor->floor.getArrivalFloorContainer()
                .stream()
                .allMatch(passenger -> floor.getNumber() ==  passenger.getDestinationFloor()));
    }
}
