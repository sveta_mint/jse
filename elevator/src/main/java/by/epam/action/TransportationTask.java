package by.epam.action;

import by.epam.beans.Building;
import by.epam.beans.Elevator;
import by.epam.beans.Floor;
import by.epam.beans.Passenger;
import by.epam.enums.TransportationActions;
import by.epam.enums.TransportationState;
import org.apache.log4j.Logger;

/**
 * Class describing transportation task passenger
 */
public class TransportationTask implements Runnable {
    private static final Logger LOGGER = Logger.getLogger(TransportationTask.class);
    private Building building;
    private Passenger passenger;
    private TransportationState transportationState;

    /**
     * Constructor for creating transportation task passenger
     * @param building building
     * @param passenger passenger
     */
    public TransportationTask(final Building building, final Passenger passenger) {
        this.building = building;
        this.passenger = passenger;
        passenger.setTransportationState(TransportationState.IN_PROGRESS);
    }

    /**
     * method that implements the logic of new threads
     */
    public void run() {
        Floor currentFloor = passenger.getDispatchFloor();
        Elevator elevator = building.getElevator();
        try {
            synchronized (currentFloor) {
                while (elevator.getCurrentFloorNumber() != passenger.getCurrentFloor()
                        || elevator.isFullCapacity() ||
                        !elevator.isSameDirection(passenger)) {
                    currentFloor.wait();
                }
            }
            building.getController().enterToElevator(passenger);
            LOGGER.info(TransportationActions.BOARDING_OF_PASSENGER + " " + passenger);
            System.out.println(TransportationActions.BOARDING_OF_PASSENGER + " " + passenger);
            synchronized (elevator) {
                while (elevator.getCurrentFloorNumber() != passenger.getDestinationFloor()) {
                    elevator.wait();
                }
            }
            building.getController().exitElevator(passenger);
            passenger.setTransportationState(TransportationState.COMPLETED);
            LOGGER.info(TransportationActions.DEBOARDING_OF_PASSENGER + " " + passenger);
            System.out.println(TransportationActions.DEBOARDING_OF_PASSENGER + " " + passenger);
        } catch (InterruptedException e) {
            LOGGER.error(e);
        }

    }

    /**
     * Getter for transportation state passenger
     * @return transportation state passenger
     */
    public TransportationState getTransportationState() {
        return transportationState;
    }

    /**
     * Setter for transportation state passenger
     * @param transportationState passenger
     */
    public void setTransportationState(final TransportationState transportationState) {
        this.transportationState = transportationState;
    }

}
