package by.epam.beans;

import by.epam.action.Controller;
import by.epam.action.TransportationTask;
import by.epam.enums.TransportationActions;
import by.epam.reader.ConfigReader;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import static by.epam.enums.Constants.*;

/**
 * Class describing the building
 */
public class Building {
    private int floorsNumber;
    private int elevatorCapacity;
    private int passengersNumber;
    private List<Floor> floors;
    private Elevator elevator;
    private Controller controller;
    private ExecutorService threadPool;
    private int arrivedPassengers;
    private List<Passenger> passengers = new ArrayList<>();
    private static Logger LOGGER = Logger.getLogger(Building.class);

    /**
     * Constructor for creating building
     */
    public Building() {
        ConfigReader configReader = ConfigReader.getConfigReader();
        passengersNumber = configReader.getPassengersNumber();
        floorsNumber = configReader.getFloorsNumber();
        elevatorCapacity = configReader.getElevatorCapacity();
        this.elevator = new Elevator(elevatorCapacity, floorsNumber);
        controller = new Controller(this);
        initFloors();
        generatePassengers(passengersNumber, floorsNumber);
    }

    private void initFloors() {
        floors = Arrays.asList(new Floor[floorsNumber]);
        for (int i = 0; i < floors.size(); i++) {
            floors.set(i, new Floor(i + ONE));
        }
    }

    private void generatePassengers(final int passengersNumber, final int floorsNumber) {
        if (passengersNumber <= 0 || floorsNumber <= 0) {
            throw new IllegalArgumentException(WRONG_INIT_DATA);
        }
        int currentFloor;
        int destinationFloor;
        Passenger passenger;
        for (int i = 1; i <= passengersNumber; i++) {
            currentFloor = 1 + (int)(Math.random() * floorsNumber);
            destinationFloor = 1 + (int)(Math.random() * floorsNumber);
            while (currentFloor == destinationFloor) {
                destinationFloor = 1 + (int)(Math.random() * floorsNumber);
            }
            passenger = new Passenger(i, currentFloor, destinationFloor);
            passenger.setDispatchFloor(floors.get(currentFloor - ONE));
            passenger.setArrivalFloor(floors.get(destinationFloor - ONE));
            floors.get(currentFloor - ONE).getDispatchFloorContainer().add(passenger);
            passengers.add(passenger);
            LOGGER.info(passengers.get(i - ONE));
            System.out.println(passengers.get(i - ONE));
        }
    }

    /**
     * method for calculating the number of passengers arriving
     * @return count arrival passengers
     */
    public synchronized int arrived() {
        return arrivedPassengers++;
    }

    /**
     * checking method all passengers arrived
     * @return check result
     */
    public synchronized boolean allArrived() {
        return arrivedPassengers == getPassengersNumber();
    }

    /**
     * method for starting transportation passengers
     * @param building building
     */
    public void startTransportation(final Building building) {
        List<Passenger> passengers = building.getPassengers();
        threadPool = Executors.newFixedThreadPool(passengers.size());
        LOGGER.info(elevator);
        System.out.println(elevator);
        for (Passenger passenger : passengers) {
            threadPool.execute(new TransportationTask(building, passenger));
        }
        controller.moveElevator(this);
        threadPool.shutdown();
        LOGGER.info(TransportationActions.COMPLETION_TRANSPORTATION);
        System.out.println(TransportationActions.COMPLETION_TRANSPORTATION);
    }

    /**
     * Getter for controller
     * @return controller
     */
    public Controller getController() {
        return controller;
    }

    /**
     * Getter for all floors in the building
     * @return list floors
     */
    public List<Floor> getFloors() {
        return floors;
    }

    /**
     * Getter for all passengers in the building
     * @return list passengers
     */
    public synchronized List<Passenger> getPassengers() {
        return passengers;
    }

    /**
     * Setter for passengers number
     * @param passengersNumber count passengers
     */
    public void setPassengersNumber(int passengersNumber) {
        this.passengersNumber = passengersNumber;
    }

    /**
     * Getter for floor number
     * @return floor number
     */
    public int getFloorsNumber() {
        return floorsNumber;
    }

    /**
     * Getter for elevator
     * @return elevator
     */
    public Elevator getElevator() {
        return elevator;
    }

    /**
     * Getter for count passengers in the building
     * @return count passengers
     */
    public int getPassengersNumber() {
        return passengersNumber;
    }

}
