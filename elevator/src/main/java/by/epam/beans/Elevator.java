package by.epam.beans;

import by.epam.enums.Direction;

import java.util.ArrayList;
import java.util.List;

/**
 * Class describing the elevator in building
 */
public class Elevator {
    private final int elevatorCapacity;
    private int currentFloorNumber;
    private int maxFloorNumber;
    private Direction direction;
    private Floor currentFloor;
    private List<Passenger> elevatorContainer;

    /**
     * Constructor for creating elevator
     * @param elevatorCapacity elevator capacity
     * @param maxFloorNumber max floor in building
     */
    public Elevator(final int elevatorCapacity, final int maxFloorNumber) {
        this.elevatorCapacity = elevatorCapacity;
        this.maxFloorNumber = maxFloorNumber;
        elevatorContainer = new ArrayList<>(elevatorCapacity);
        currentFloorNumber = 1;
        direction = Direction.UP;
    }

    /**
     * check full capacity elevator
     * @return check result
     */
    public boolean isFullCapacity()  {
        return elevatorContainer.size() == elevatorCapacity;
    }

    /**
     * checking for matching direction of passengers and elevator
     * @param passenger passenger
     * @return check result
     */
    public boolean isSameDirection(final Passenger passenger) {
        return getDirection() == passenger.getDirection();
    }

    /**
     * choosing the direction of the elevator
     */
    private Direction chooseDirection() {
        if (currentFloorNumber == 1) {
            direction = Direction.UP;
        } else {
            if (currentFloorNumber == maxFloorNumber)
                direction = Direction.DOWN;
        }
        return direction;
    }

    /**
     * Calculation of the next floor depending on the direction of the elevator
     * @return next floor
     */
    public int nextFloor() {
        if (getDirection() == Direction.UP) {
            currentFloorNumber++;
        } else {
            currentFloorNumber--;
        }
        chooseDirection();
        return currentFloorNumber;
    }


    /**
     * Getter for elevator capacity
     * @return elevator capacity
     */
    public int getElevatorCapacity() {
        return elevatorCapacity;
    }

    /**
     * Getter for current floor object
     * @return floor
     */
    public Floor getCurrentFloor() {
        return currentFloor;
    }

    /**
     * Setter for for current floor object
     * @param currentFloor floor
     */
    public void setCurrentFloor(final Floor currentFloor) {
        this.currentFloor = currentFloor;
    }

    /**
     * Getter for current floor number
     * @return current floor number
     */
    public int getCurrentFloorNumber() {
        return currentFloorNumber;
    }

    /**
     * Setter for current floor number
     * @param currentFloorNumber floor number
     */
    public void setCurrentFloorNumber(final int currentFloorNumber) {
        this.currentFloorNumber = currentFloorNumber;
    }

    /**
     * Getter for max floor number in building
     * @return max floor number
     */
    public int getMaxFloorNumber() {
        return maxFloorNumber;
    }

    /**
     * Setter for max floor number in building
     * @param maxFloorNumber floor number
     */
    public void setMaxFloorNumber(final int maxFloorNumber) {
        this.maxFloorNumber = maxFloorNumber;
    }

    /**
     * Getter for elevator direction
     * @return elevator direction
     */
    public Direction getDirection() {
        return direction;
    }

    /**
     * Setter for elevator direction
     * @param direction elevator
     */
    public void setDirection(final Direction direction) {
        this.direction = direction;
    }

    /**
     * Getter for passenger in elevator
     * @return List passenger
     */
    public List<Passenger> getElevatorContainer() {
        return elevatorContainer;
    }

    /**
     * Setter for passenger in elevator
     * @param elevatorContainer container
     */
    public void setElevatorContainer(final List<Passenger> elevatorContainer) {
        this.elevatorContainer = elevatorContainer;
    }

    @Override
    public String toString() {
        return "ELEVATOR CAPACITY IS " + elevatorCapacity;
    }
}
