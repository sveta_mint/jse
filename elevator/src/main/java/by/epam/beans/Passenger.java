package by.epam.beans;

import by.epam.enums.Direction;
import by.epam.enums.TransportationState;

/**
 * Class describing the passenger in the building
 */
public class Passenger {
    private int passengerID;
    private int currentFloor;
    private int destinationFloor;
    private TransportationState transportationState;
    private Direction direction;
    private Floor dispatchFloor;
    private Floor arrivalFloor;

    /**
     * Constructor for creating passenger
     * @param passengerID unique passenger number
     * @param currentFloor passenger
     * @param destinationFloor passenger
     */
    public Passenger(final int passengerID, final int currentFloor, final int destinationFloor) {
        this.passengerID = passengerID;
        this.currentFloor = currentFloor;
        this.destinationFloor = destinationFloor;
        this.transportationState = TransportationState.NOT_STARTED;
        if (destinationFloor < currentFloor) {
            direction = Direction.DOWN;
        } else {
            direction = Direction.UP;
        }
    }

    @Override
    public String toString() {
        return "PASSENGER# " + passengerID + " CURRENT FLOOR " + currentFloor + " DESTINATION FLOOR "
                + destinationFloor + "; " + transportationState;
    }

    /**
     * Getter for dispatch floor object
     * @return Floor dispatch floor
     */

    public Floor getDispatchFloor() {
        return dispatchFloor;
    }

    /**
     * Setter for dispatch floor object
     * @param dispatchFloor floor object
     */
    public void setDispatchFloor(final Floor dispatchFloor) {
        this.dispatchFloor = dispatchFloor;
    }

    /**
     * Getter for arrival floor object
     * @return arrivalFloor object
     */

    public Floor getArrivalFloor() {
        return arrivalFloor;
    }

    /**
     * Setter for  arrival floor object
     * @param arrivalFloor floor object
     */

    public void setArrivalFloor(final Floor arrivalFloor) {
        this.arrivalFloor = arrivalFloor;
    }

    /**
     * Getter for number current floor passenger
     * @return current floor number
     */

    public int getCurrentFloor() {
        return currentFloor;
    }

    /**
     * Setter for number current floor passenger
     * @param currentFloor number
     */

    public void setCurrentFloor(final int currentFloor) {
        this.currentFloor = currentFloor;
    }

    /**
     * Getter for number destination floor passenger
     * @return number destination floor
     */

    public int getDestinationFloor() {
        return destinationFloor;
    }

    /**
     * Setter for number destination floor passenger
     * @param destinationFloor number
     */

    public void setDestinationFloor(final int destinationFloor) {
        this.destinationFloor = destinationFloor;
    }

    /**
     * Getter for Transportation State passenger
     * @return transportationState passenger
     */

    public TransportationState getTransportationState() {
        return transportationState;
    }

    /**
     * Setter for Transportation State passenger
     * @param transportationState passenger
     */
    public void setTransportationState(final TransportationState transportationState) {
        this.transportationState = transportationState;
    }

    /**
     * Getter for Transportation State passenger
     * @return transportationState passenger
     */

    public Direction getDirection() {
        return direction;
    }

    /**
     * Setter for direction passenger
     * @param direction passenger
     */

    public void setDirection(final Direction direction) {
        this.direction = direction;
    }

    /**
     * Getter for passenger id
     * @return unique passenger number
     */

    public int getPassengerID() {
        return passengerID;
    }

    /**
     * Setter for passenger id
     * @param passengerID unique passenger number
     */
    public void setPassengerID(final int passengerID) {
        this.passengerID = passengerID;
    }
}
