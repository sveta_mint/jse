package by.epam.beans;
import java.util.ArrayList;
import java.util.List;
/**
 * Class describing the floor of the building
 */

public class Floor {
    private int number;
    private List<Passenger> dispatchFloorContainer = new ArrayList<Passenger>();
    private List<Passenger> arrivalFloorContainer = new ArrayList<Passenger>();

    /**
     * Constructor to create a floor object.
     * @param number takes the floor number to the entrance
     */

    public Floor(final int number) {
        this.number = number;
    }

    /**
     * Getter for floor number
     * @return number floor
     */

    public int getNumber() {
        return number;
    }

    /**
     * setter for floor number
     * @param number floor
     */

    public void setNumber(final int number) {
        this.number = number;
    }

    /**
     * Getter for dispatch container on floor
     * @return List passengers
     */

    public List<Passenger> getDispatchFloorContainer() {
        return dispatchFloorContainer;
    }

    /**
     * Setter for dispatch container on floor
     * @param dispatchFloorContainer List passengers
     */

    public void setDispatchFloorContainer(final List<Passenger> dispatchFloorContainer) {
        this.dispatchFloorContainer = dispatchFloorContainer;
    }

    /**
     * Getter for Arrival container on floor
     * @return List passengers
     */

    public List<Passenger> getArrivalFloorContainer() {
        return arrivalFloorContainer;
    }

    /**
     * Setter for arrival container on floor
     * @param arrivalFloorContainer List passengers
     */

    public void setArrivalFloorContainer(final List<Passenger> arrivalFloorContainer) {
        this.arrivalFloorContainer = arrivalFloorContainer;
    }

    @Override
    public String toString() {
        return "FLOOR " + number;
    }
}
