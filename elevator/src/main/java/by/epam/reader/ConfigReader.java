package by.epam.reader;

import org.apache.log4j.Logger;

import java.util.MissingResourceException;
import java.util.ResourceBundle;
import static by.epam.enums.Constants.*;

/**
 * Class for reading parameters from config file
 */
public final class ConfigReader {
    private static ConfigReader configReader;
    private int floorsNumber;
    private int elevatorCapacity;
    private int passengersNumber;
    private static ResourceBundle resourceBundle;
    private static final Logger LOGGER = Logger.getLogger(ConfigReader.class);

    private ConfigReader() {
        try {
            init();
        } catch (MissingResourceException e) {
            System.out.println(CONFIG_FILE_NOT_FOUND + "  " + e);
            LOGGER.error(CONFIG_FILE_NOT_FOUND, e);
        }

    }

    /**
     * Static method for creating one object configReader
     * @return configReader object
     */
    public static ConfigReader getConfigReader() {
        if (configReader == null) {
            configReader = new ConfigReader();
        }
        return configReader;
    }

    /**
     * method for obtaining init settings from a config file
     * @throws MissingResourceException if file not found
     */
    public void init() throws MissingResourceException {
        resourceBundle = ResourceBundle.getBundle(NAME_FILE);
        setFloorsNumber(Integer.parseInt(resourceBundle.getString(FLOORS_NUMBERS)));
        setElevatorCapacity(Integer.parseInt(resourceBundle.getString(ELEVATOR_CAPACITY)));
        setPassengersNumber(Integer.parseInt(resourceBundle.getString(PASSENGER_NUMBER)));
    }

    /**
     * Getter for init floors number
     * @return floors namber
     */
    public int getFloorsNumber() {
        return floorsNumber;
    }

    /**
     * Setter for floors number
     * @param floorsNumber floors number in building
     */
    public void setFloorsNumber(final int floorsNumber) {
        this.floorsNumber = floorsNumber;
    }

    /**
     * Getter for init elevator capacity
     * @return elevator capacity
     */
    public int getElevatorCapacity() {
        return elevatorCapacity;
    }

    /**
     * Setter for elevator capacity
     * @param elevatorCapacity Elevator Capacity
     */
    public void setElevatorCapacity(final int elevatorCapacity) {
        this.elevatorCapacity = elevatorCapacity;
    }

    /**
     * Getter for init passengers number
     * @return passengers number
     */
    public int getPassengersNumber() {
        return passengersNumber;
    }

    /**
     * Setter for passengers number
     * @param passengersNumber passenger number in building
     */
    public void setPassengersNumber(final int passengersNumber) {
        this.passengersNumber = passengersNumber;
    }

}
