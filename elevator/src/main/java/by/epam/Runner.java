package by.epam;

import by.epam.beans.Building;

/**
 * Class for starting application
 */
public final class Runner {
    /**
     * entry point into the application
     * @param args string array
     */
    public static void main(final String[] args) {
        Building building = new Building();
        building.startTransportation(building);
    }

    private Runner() {

    }
}
